/** @type {import("prettier").Config} */
module.exports = {
  semi: false,
  trailingComma: 'es5',
  singleQuote: true,
  tabWidth: 2,
  useTabs: false,
  endOfLine: 'auto',
  jsxSingleQuote: false,
  arrowParens: 'avoid',
  bracketSpacing: true,
  printWidth: 80,
  bracketSameLine: true,
  plugins: [require.resolve('prettier-plugin-tailwindcss')],
}
