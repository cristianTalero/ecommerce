import { create } from 'zustand'

type Product = {
  id: string
  title: string
  image: string
  price: number
  units: number
  max: number
}

type CartState = {
  items: Product[]
  computed: {
    get isEmpty(): boolean
    get total(): number
  }

  add(_: Product): void
  wasAdded(_: string): boolean
  increase(_: string): void
  isMax(_: string): boolean
  remove(_: string): void
  removeAll(): void
}

export const useCartStore = create<CartState>((set, get) => ({
  items: [],
  computed: {
    get isEmpty() {
      return get().items.length === 0
    },
    get total() {
      return get().items.reduce((total, product) => {
        return total + product.price * product.units
      }, 0)
    },
  },

  add: (product: Product) =>
    set(state => {
      const max = product.max > 0 && product.units > product.max

      if (product.units < 1 || max) {
        return state
      }

      return { ...state, items: [product, ...state.items] }
    }),
  wasAdded: (id: string) => {
    return get().items.some(item => item.id === id)
  },
  increase: (id: string) =>
    set(state => ({
      items: state.items.map(item => {
        if (item.id !== id) return item

        return {
          ...item,
          units: item.units + 1,
        }
      }),
    })),
  isMax: (id: string) => {
    const item = get().items.find(item => item.id === id)

    if (item) {
      if (item.max > 0 && item.units >= item.max) {
        return true
      }
    }

    return false
  },
  remove: (id: string) =>
    set(state => ({
      items: state.items
        .map(item => {
          if (item.id !== id) return item

          return {
            ...item,
            units: item.units - 1,
          }
        })
        .filter(item => item.units > 0),
    })),
  removeAll: () => set(state => ({ ...state, items: [] })),
}))
