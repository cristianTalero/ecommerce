declare namespace jest {
  interface Matchers<_> {
    toHaveTextContent: (_: string) => object
    toBeInTheDOM: () => void
  }

  interface Expect {
    toHaveTextContent: (_: string) => object
    toBeInTheDOM: () => void
  }
}
