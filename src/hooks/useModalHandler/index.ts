import { useEffect, useState } from 'react'

export function useModalHandler(open: boolean) {
  const [show, setShow] = useState(false)

  useEffect(() => {
    setShow(open)

    if (open) {
      document.documentElement.style.overflow = 'hidden'
    } else {
      document.documentElement.style.overflow = ''
    }
  }, [open])

  return show
}
