import { useSession } from 'next-auth/react'
import { useRouter } from 'next/router'

export function useAuthRedirect() {
  const session = useSession({ required: false })
  const router = useRouter()

  if (session.status === 'authenticated') {
    router.replace('/')
  }
}
