import { useSession } from 'next-auth/react'
import { useRouter } from 'next/router'
import { useEffect } from 'react'
import { useCartStore } from '~/store/cartStore'

export function useRequiredCart() {
  useSession({ required: true })

  const { noProducts } = useCartStore(cart => ({
    noProducts: cart.computed.isEmpty,
  }))

  const router = useRouter()

  useEffect(() => {
    if (noProducts) {
      router.replace('/')
    }
  }, [noProducts])
}
