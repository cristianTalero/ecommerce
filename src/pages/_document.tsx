import { Html, Head, Main, NextScript } from 'next/document'
import { env } from '~/env/server.mjs'

const isDev = env.NODE_ENV !== 'production'

function MyDocument() {
  return (
    <Html lang="en">
      <Head>
        <meta name="theme-color" content="#fff" />
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="manifest" href="/manifest.webmanifest" />
        <link rel="apple-touch-icon" href="/icons/icon-192x192.png" />
        <meta name="theme-color" content="#a02219" />
      </Head>

      <body className={isDev ? 'debug-screens' : ''}>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}

export default MyDocument
