import { type NextPage } from 'next'

import { SectionHeader } from '~/components/section/SectionHeader'
import { ProductCard } from '~/components/product/ProductCard'
import { ProductSectionMore } from '~/components/product/ProductSectionMore'
import { keys } from '~/utils/products'
import { Hero } from '~/components/layout/Hero'
import { MainLayout } from '~/layouts/MainLayout'

import { ProductCategoryCard } from '~/components/product/ProductCategoryCard'
import { api } from '~/utils/api'
import { SectionFetchError } from '~/components/section/SectionFetchError'
import { SectionNoContent } from '~/components/section/SectionNoContent'
import { ProductCardLoading } from '~/components/product/ProductCard/loading'

const Home: NextPage = () => {
  const clothes = api.product.getByCategory.useQuery(
    { category: 'clothes' },
    { refetchOnWindowFocus: false }
  )

  const technology = api.product.getByCategory.useQuery(
    { category: 'technology' },
    { refetchOnWindowFocus: false }
  )

  const sports = api.product.getByCategory.useQuery(
    { category: 'sports' },
    { refetchOnWindowFocus: false }
  )

  const home = api.product.getByCategory.useQuery(
    { category: 'home' },
    { refetchOnWindowFocus: false }
  )

  const toys = api.product.getByCategory.useQuery(
    { category: 'toys' },
    { refetchOnWindowFocus: false }
  )

  return (
    <MainLayout title="Feed">
      <Hero category={null} />

      <main className="px-2 py-4">
        <SectionHeader title="Categories" id="categories" />
        <section className="scroll-section">
          {keys.map(category => (
            <ProductCategoryCard key={category} category={category as never} />
          ))}
        </section>

        <SectionHeader title="Clothes" id="clothes" />
        <section className="scroll-section">
          {clothes.isLoading
            ? [0, 1, 2, 3, 4].map(item => <ProductCardLoading key={item} />)
            : null}

          {clothes.data?.length === 0 ? (
            <SectionNoContent section="Products" />
          ) : null}

          {clothes.isError ? (
            <SectionFetchError error={clothes.error.message} />
          ) : null}

          {clothes.data &&
            clothes.data.map(product => (
              <ProductCard
                key={product.id}
                id={product.id}
                image={product.image}
                category={product.category}
                title={product.title}
                description={product.description}
                price={product.price}
                owner={product.owner.image}
              />
            ))}
          <ProductSectionMore category="clothes" />
        </section>

        <SectionHeader title="Home" id="home" />
        <section className="scroll-section">
          {home.isLoading
            ? [0, 1, 2, 3, 4].map(item => <ProductCardLoading key={item} />)
            : null}

          {home.data?.length === 0 ? (
            <SectionNoContent section="Products" />
          ) : null}

          {home.isError ? (
            <SectionFetchError error={home.error.message} />
          ) : null}

          {home.data &&
            home.data.map(product => (
              <ProductCard
                key={product.id}
                id={product.id}
                image={product.image}
                category={product.category}
                title={product.title}
                description={product.description}
                price={product.price}
                owner={product.owner.image}
              />
            ))}
          <ProductSectionMore category="home" />
        </section>

        <SectionHeader title="Sports" id="sports" />
        <section className="scroll-section">
          {sports.isLoading
            ? [0, 1, 2, 3, 4].map(item => <ProductCardLoading key={item} />)
            : null}

          {sports.data?.length === 0 ? (
            <SectionNoContent section="Products" />
          ) : null}

          {sports.isError ? (
            <SectionFetchError error={sports.error.message} />
          ) : null}

          {sports.data &&
            sports.data.map(product => (
              <ProductCard
                key={product.id}
                id={product.id}
                image={product.image}
                category={product.category}
                title={product.title}
                description={product.description}
                price={product.price}
                owner={product.owner.image}
              />
            ))}
          <ProductSectionMore category="sports" />
        </section>

        <SectionHeader title="Technology" id="technology" />
        <section className="scroll-section">
          {technology.isLoading
            ? [0, 1, 2, 3, 4].map(item => <ProductCardLoading key={item} />)
            : null}

          {technology.data?.length === 0 ? (
            <SectionNoContent section="Products" />
          ) : null}

          {technology.isError ? (
            <SectionFetchError error={technology.error.message} />
          ) : null}

          {technology.data &&
            technology.data.map(product => (
              <ProductCard
                key={product.id}
                id={product.id}
                image={product.image}
                category={product.category}
                title={product.title}
                description={product.description}
                price={product.price}
                owner={product.owner.image}
              />
            ))}
          <ProductSectionMore category="technology" />
        </section>

        <SectionHeader title="Toys" id="toys" />
        <section className="scroll-section">
          {toys.isLoading
            ? [0, 1, 2, 3, 4].map(item => <ProductCardLoading key={item} />)
            : null}

          {toys.data?.length === 0 ? (
            <SectionNoContent section="Products" />
          ) : null}

          {toys.isError ? (
            <SectionFetchError error={toys.error.message} />
          ) : null}

          {toys.data &&
            toys.data.map(product => (
              <ProductCard
                key={product.id}
                id={product.id}
                image={product.image}
                category={product.category}
                title={product.title}
                description={product.description}
                price={product.price}
                owner={product.owner.image}
              />
            ))}
          <ProductSectionMore category="toys" />
        </section>
      </main>
    </MainLayout>
  )
}

export default Home
