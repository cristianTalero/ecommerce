import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { Hero } from '~/components/layout/Hero'
import { SectionHeader } from '~/components/section/SectionHeader'
import { ProductCard } from '~/components/product/ProductCard'
import { MainLayout } from '~/layouts/MainLayout'
import type { Category } from '~/utils/products'
import { api } from '~/utils/api'
import { SectionFetchError } from '~/components/section/SectionFetchError'
import { SectionNoContent } from '~/components/section/SectionNoContent'
import { ProductCardLoading } from '~/components/product/ProductCard/loading'

const Products: NextPage = () => {
  const { query } = useRouter()
  const category = query.category as Category

  const products = api.product.getByCategory.useQuery(
    { category: category ? category : null },
    { refetchOnWindowFocus: false }
  )

  return (
    <MainLayout title="Products">
      <Hero category={category as never} />

      <main className="px-2 py-4">
        <SectionHeader title="Products" id="products" />
        <section
          style={{
            display: 'grid',
            gridTemplateColumns: 'repeat(auto-fit, minmax(18rem, 1fr))',
            placeItems: 'start',
            placeContent: 'center',
            gap: '1rem',
            padding: '1rem',
          }}>
          {products.isLoading
            ? [0, 1, 2, 3, 4].map(item => <ProductCardLoading key={item} />)
            : null}

          {products.data?.length === 0 ? (
            <SectionNoContent section="Products" />
          ) : null}

          {products.isError ? (
            <SectionFetchError error={products.error.message} />
          ) : null}

          {products.data &&
            products.data.map(product => (
              <ProductCard
                key={product.id}
                id={product.id}
                image={product.image}
                category={product.category}
                title={product.title}
                description={product.description}
                price={product.price}
                owner={product.owner.image}
              />
            ))}
        </section>
      </main>
    </MainLayout>
  )
}

export default Products
