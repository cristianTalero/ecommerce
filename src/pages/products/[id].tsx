import type { Product, Review } from '@prisma/client'
import type { NextPage, NextPageContext } from 'next'
import Link from 'next/link'
import { useCallback, useState } from 'react'
import { Button, Divider, Form, Input } from 'react-daisyui'
import { BsCartFill } from 'react-icons/bs'
import { shallow } from 'zustand/shallow'
import { Avatar } from '~/components/utils/Avatar'
import { CustomImage } from '~/components/utils/CustomImage'
import { ShowMoreText } from '~/components/utils/ShowMoreText'
import { MainLayout } from '~/layouts/MainLayout'
import { useCartStore } from '~/store/cartStore'
import { categories } from '~/utils/products'
import { toCurrency } from '~/utils/toCurrency'
import { prisma } from '~/server/db'
import { SectionHeader } from '~/components/section/SectionHeader'
import { RatingCard } from '~/components/rating/RatingCard'
import { RatingReview } from '~/components/rating/RatingReview'
import { RatingForm } from '~/components/rating/RatingForm'

import { RatingOwnReview } from '~/components/rating/RatingOwnReview'
import { api } from '~/utils/api'
import { SectionNoContent } from '~/components/section/SectionNoContent'
import { SectionFetchError } from '~/components/section/SectionFetchError'
import { RatingReviewLoading } from '~/components/rating/RatingReview/loading'
import { getServerAuthSession } from '~/server/auth'
import { RatingCardLoading } from '~/components/rating/RatingCard/loading'
import { RatingFormLoading } from '~/components/rating/RatingForm/loading'
import { useSession } from 'next-auth/react'

export async function getServerSideProps(ctx: NextPageContext) {
  const id = ctx.query.id as string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const session = await getServerAuthSession(ctx as any)
  const userId = session?.user?.id

  const product = await prisma.product.findFirst({
    where: { id },
    include: {
      owner: { select: { image: true } },
      reviews: { select: { ownerId: true } },
    },
  })

  if (!product) {
    return { notFound: true }
  }

  const alreadyRated = product.reviews.find(review => review.ownerId === userId)

  let ownReview: Review | null = null
  if (alreadyRated) {
    ownReview = await prisma.review.findFirst({
      where: { ownerId: alreadyRated.ownerId, productId: product.id },
      include: { owner: { select: { id: true, name: true, image: true } } },
    })
  }

  return { props: { ...product, ownReview } }
}

type OwnReviewType = {
  owner: {
    id: string
    name: string
    image: string | null
  }
} & Review

type PageProps = {
  owner: { image: string }
  ownReview: OwnReviewType | null
} & Product

const Page: NextPage<PageProps> = ({
  id,
  title,
  category,
  image,
  description,
  max_units,
  price,
  ownerId,
  owner,
  ownReview,
}) => {
  const [units, setUnits] = useState(1)
  const { status } = useSession()

  const { addProductToCart, wasProductAdded, removeAllProducts } = useCartStore(
    cart => ({
      addProductToCart: cart.add,
      wasProductAdded: cart.wasAdded(id),
      removeAllProducts: cart.removeAll,
    }),
    shallow
  )

  const cartHandler = () => {
    if (wasProductAdded) {
      return removeAllProducts()
    }

    addProductToCart({
      id,
      image,
      price,
      title,
      units,
      max: max_units ?? 0,
    })
  }

  const reviews = api.reviews.getAll.useQuery(
    {
      productId: id,
    },
    { refetchOnWindowFocus: false }
  )

  const getTotalReviews = useCallback(
    (stars?: number) => {
      const list = reviews.data

      if (list && stars) {
        const total = list.filter(review => review.rate === stars)
        return total.length
      } else if (list) return list.length

      return 0
    },
    [reviews.data]
  )

  return (
    <MainLayout title={title}>
      <main className="p-4">
        <section className="m-auto flex w-full flex-col justify-center gap-4 md:w-4/5 lg:flex-row">
          <picture className="relative h-80 w-full lg:h-96 lg:w-1/2">
            <CustomImage src={image} alt="Product image" fill />
          </picture>
          <div className="flex flex-col gap-4 lg:w-1/2">
            <h3 className="flex items-center gap-2 text-lg font-bold capitalize text-secondary">
              {categories[category]}
              {category}
            </h3>
            <h1 className="text-4xl font-black">{title}</h1>

            <ShowMoreText className="text-2xl" content={description} />

            <Form className="text-sm font-semibold text-label">
              <Form.Label title="Select units">
                {max_units ? `Max. ${max_units}` : null}
              </Form.Label>
              <Input
                value={units}
                borderOffset
                bordered
                type="number"
                min={1}
                max={5}
                onChange={e => setUnits(Number(e.target.value))}
              />
            </Form>
            <Divider className="m-0" />
            <div className="flex flex-wrap items-center justify-start gap-7">
              <h2 className="text-3xl font-bold text-success">
                {toCurrency(price)}
              </h2>
              <Link
                href={`/users/${ownerId}`}
                className="touchable"
                aria-label="Owner profile">
                <Avatar src={owner.image} alt="Product owner" size={50} />
              </Link>

              {status === 'authenticated' ? (
                <Button
                  className="grow"
                  onClick={cartHandler}
                  variant={wasProductAdded ? 'outline' : undefined}
                  startIcon={<BsCartFill />}
                  color="primary">
                  {!wasProductAdded ? 'Add to cart' : 'Remove from cart'}
                </Button>
              ) : (
                <Link href="/auth/signin" className="btn-primary btn grow">
                  Signin
                </Link>
              )}
            </div>
          </div>
        </section>
        <Divider />

        <SectionHeader id="reviews" title="Reviews" />
        <section className="lg flex flex-col gap-3 lg:flex-row lg:items-baseline lg:justify-center">
          {ownReview ? (
            <RatingOwnReview
              productId={id}
              reviewId={ownReview.id}
              message={ownReview.message}
              rate={ownReview.rate}>
              <RatingReview
                owner={ownReview.owner}
                message={ownReview.message}
                rate={ownReview.rate}
                createdAt={ownReview.createdAt}
                updatedAt={ownReview.updatedAt}
              />
            </RatingOwnReview>
          ) : status === 'authenticated' ? (
            <RatingForm id={id} />
          ) : status === 'loading' ? (
            <RatingFormLoading />
          ) : null}

          {reviews.data ? (
            <RatingCard
              total={getTotalReviews()}
              total1={getTotalReviews(1)}
              total2={getTotalReviews(2)}
              total3={getTotalReviews(3)}
              total4={getTotalReviews(4)}
              total5={getTotalReviews(5)}
            />
          ) : (
            <RatingCardLoading />
          )}
        </section>
        <Divider />

        <section className="m-auto flex w-full flex-col gap-4 lg:w-10/12">
          {reviews.isLoading
            ? [0, 1, 2, 3, 4].map(item => <RatingReviewLoading key={item} />)
            : null}

          {reviews.data?.length === 0 ? (
            <SectionNoContent section="Reviews" />
          ) : null}

          {reviews.isError ? (
            <SectionFetchError error={reviews.error.message} />
          ) : null}

          {reviews.data &&
            reviews.data.map(review => (
              <RatingReview
                key={review.id}
                owner={review.owner}
                message={review.message}
                rate={review.rate}
                createdAt={review.createdAt}
                updatedAt={review.updatedAt}
              />
            ))}
        </section>
      </main>
    </MainLayout>
  )
}

export default Page
