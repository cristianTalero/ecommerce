import { ErrorMessage } from '@hookform/error-message'
import { zodResolver } from '@hookform/resolvers/zod'
import type { NextPage } from 'next'
import { signIn } from 'next-auth/react'
import Link from 'next/link'

import { Button, Card, Divider, Form, Input, InputGroup } from 'react-daisyui'
import { type FieldValues, useForm } from 'react-hook-form'
import { MdMail } from 'react-icons/md'
import { z } from 'zod'

import { StaticLayout } from '~/layouts/StaticLayout'
import { SocialButton } from '~/components/utils/SocialButton'
import { Suspense, useCallback, useState } from 'react'
import dynamic from 'next/dynamic'
import { useAuthRedirect } from '~/hooks/useAuthRedirect'

const AsyncLoading = dynamic(async () => {
  const asyncLoading = await import('~/components/utils/AsyncLoading')
  return asyncLoading.AsyncLoading
})

const emailSchema = z
  .object({
    email: z.string().trim().email("It's not a valid email!"),
  })
  .strict()

const SignIn: NextPage = () => {
  const [loading, setLoading] = useState(false)
  useAuthRedirect()

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(emailSchema),
  })

  const signInWithEmail = (e: FieldValues) => {
    setLoading(true)
    signIn('email', { email: e.email })
  }

  const signInWithGoogle = useCallback(() => {
    setLoading(true)
    signIn('google')
  }, [])

  const signInWithFacebook = useCallback(() => {
    setLoading(true)
    signIn('facebook')
  }, [])

  return (
    <StaticLayout title="Sign In">
      {loading ? (
        <Suspense>
          <AsyncLoading />
        </Suspense>
      ) : null}

      <Card className="sized-card bg-base-100" bordered>
        <Card.Body>
          <h1 className="card-title mb-4 text-2xl font-black">
            Welcome to Ecommerce!
          </h1>

          <form
            onSubmit={handleSubmit(signInWithEmail)}
            className="flex flex-col justify-center gap-3">
            <div className="form-group">
              <Form.Label
                title="Email"
                htmlFor="email"
                className="font-semibold"
              />

              <>
                <InputGroup>
                  <span>
                    <MdMail />
                  </span>
                  <Input
                    bordered
                    type="email"
                    id="email"
                    color={errors['email'] ? 'error' : undefined}
                    placeholder="john_doe@mail.com"
                    className="w-full"
                    {...register('email')}
                  />
                </InputGroup>

                {errors['email'] ? (
                  <ErrorMessage
                    errors={errors}
                    name="email"
                    render={({ message }) => (
                      <span className="text-xs font-bold text-error">
                        {message}
                      </span>
                    )}
                  />
                ) : (
                  <span className="text-xs text-label">
                    We&apos;ll send you magic link
                  </span>
                )}
              </>
            </div>

            <Button
              aria-label="Email Signin options"
              type="submit"
              fullWidth
              color="primary"
              className="mt-1 normal-case">
              Sign In
            </Button>

            <Link
              href="/terms-and-conditions"
              className="touchable text-center font-bold">
              You&apos;re accepting Terms and conditions
            </Link>

            <Divider>Or</Divider>

            <div className="flex flex-col gap-2">
              <SocialButton provider="google" onClick={signInWithGoogle} />
              <SocialButton provider="facebook" onClick={signInWithFacebook} />
            </div>
          </form>
        </Card.Body>
      </Card>
    </StaticLayout>
  )
}

export default SignIn
