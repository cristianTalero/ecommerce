import type { NextPage, NextPageContext } from 'next'
import Link from 'next/link'
import { Card, Divider } from 'react-daisyui'
import { MdErrorOutline, MdLogin } from 'react-icons/md'
import { AppURI } from '~/components/utils/AppURI'
import { StaticLayout } from '~/layouts/StaticLayout'

type AuthError = 'Configuration' | 'AccessDenied' | 'Verification' | 'Default'

const errors = {
  Configuration: {
    name: 'Server error',
    description: 'There is a problem with the server configuration!',
  },
  AccessDenied: {
    name: 'Access Denied',
    description: 'You do not have permissions to sign in!',
  },
  Verification: {
    name: 'Unable to sign in',
    description:
      'The magic link is not longer valid. It may have been used already or it may have expired!',
  },
  Default: {
    name: 'Error',
    description: 'An unknown error ocurred!',
  },
}

export function getServerSideProps(ctx: NextPageContext) {
  const error = ctx.query.error
  const errorTypes = Object.keys(errors)

  if (!error || !errorTypes.includes(error as string)) {
    return { props: errors.Default }
  }

  return { props: errors[error as AuthError] }
}

type ErrorProps = {
  name: string
  description: string
}

const Error: NextPage<ErrorProps> = ({ name, description }) => {
  return (
    <StaticLayout title={name}>
      <Card className="w-[30rem] bg-base-100 shadow-xl" bordered>
        <Card.Body className="flex flex-col items-center">
          <MdErrorOutline size={200} className="text-error drop-shadow-lg" />
          <h1 className="card-title text-3xl font-black">{name}</h1>
          <p className="my-2 text-center text-xl">{description}</p>

          <AppURI />

          <Divider className="m-0" />

          <Card.Actions className="w-full">
            <Link
              href="/auth/signin"
              className="btn-outline btn-secondary btn-block btn">
              <MdLogin size={18} />
              <span className="ml-1">Sign in</span>
            </Link>
          </Card.Actions>
        </Card.Body>
      </Card>
    </StaticLayout>
  )
}

export default Error
