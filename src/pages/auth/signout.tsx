import type { NextPage } from 'next'
import { Button, Card, Divider } from 'react-daisyui'
import { BiCheck } from 'react-icons/bi'
import { MdCancel } from 'react-icons/md'
import { useRouter } from 'next/router'
import { signOut, useSession } from 'next-auth/react'
import { AppURI } from '~/components/utils/AppURI'
import { StaticLayout } from '~/layouts/StaticLayout'

const Signout: NextPage = () => {
  const router = useRouter()
  useSession({ required: true })

  return (
    <StaticLayout title="Sign Out">
      <Card className="sized-card bg-base-100 py-3" bordered>
        <Card.Body className="flex flex-col items-center">
          <h1 className="card-title text-3xl font-black">Signout</h1>
          <p className="mt-2 text-xl">Are you sure you want to sign out?</p>
          <AppURI />

          <Divider className="m-0" />

          <Card.Actions>
            <Button
              onClick={() => router.back()}
              variant="outline"
              startIcon={<MdCancel size={18} />}>
              Cancel
            </Button>

            <Button
              color="ghost"
              aria-label="Signout"
              onClick={() => signOut({ redirect: true })}>
              <BiCheck size={25} className="text-secondary" />
            </Button>
          </Card.Actions>
        </Card.Body>
      </Card>
    </StaticLayout>
  )
}

export default Signout
