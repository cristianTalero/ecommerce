import type { NextPage } from 'next'
import Link from 'next/link'
import { Card, Divider } from 'react-daisyui'
import { IoMailOpenOutline, IoReturnUpBack } from 'react-icons/io5'
import { AppURI } from '~/components/utils/AppURI'
import { useAuthRedirect } from '~/hooks/useAuthRedirect'
import { StaticLayout } from '~/layouts/StaticLayout'

const VerifyEmail: NextPage = () => {
  useAuthRedirect()

  return (
    <StaticLayout title="Verify your email">
      <Card className="sized-card bg-base-100" bordered>
        <Card.Body className="flex flex-col items-center">
          <IoMailOpenOutline
            size={200}
            className="text-secondary-focus drop-shadow-lg"
          />
          <h1 className="card-title text-3xl font-black">Verify your email</h1>
          <p className="mt-2 text-xl">
            We sent you a mail, confirm it to get into Manlou!
          </p>
          <AppURI />

          <Divider className="m-0" />

          <Card.Actions className="w-full">
            <Link href="/auth/signin" className="btn-outline btn-block btn">
              <IoReturnUpBack size={18} />
              <span className="ml-1">Return to SignIn</span>
            </Link>
          </Card.Actions>
        </Card.Body>
      </Card>
    </StaticLayout>
  )
}

export default VerifyEmail
