import type { NextPage, NextPageContext } from 'next'

import { Button, Divider, RadialProgress } from 'react-daisyui'
import { MdDelete, MdPerson } from 'react-icons/md'
import { SectionHeader } from '~/components/section/SectionHeader'
import { PurchasedProductCard } from '~/components/product/PurchasedProductCard'
import { ProductCard } from '~/components/product/ProductCard'
import { Avatar } from '~/components/utils/Avatar'
import { MainLayout } from '~/layouts/MainLayout'
import clsx from 'clsx'
import { getServerAuthSession } from '~/server/auth'
import type { User } from '@prisma/client'
import { api } from '~/utils/api'
import { SectionNoContent } from '~/components/section/SectionNoContent'
import { ProductCardLoading } from '~/components/product/ProductCard/loading'
import { SectionFetchError } from '~/components/section/SectionFetchError'
import { prisma } from '~/server/db'
import { useEffect } from 'react'
import Swal from 'sweetalert2'
import { useRouter } from 'next/router'

export async function getServerSideProps(ctx: NextPageContext) {
  const id = ctx.query.id as string

  const session = await getServerAuthSession(ctx as never)
  if (session?.user?.id === id) {
    return {
      props: session.user,
    }
  }

  const user = await prisma.user.findFirst({
    where: { id },
  })

  if (!user) {
    return {
      notFound: true,
    }
  }

  return {
    props: user,
  }
}

const Page: NextPage<User> = ({ id, name, image }) => {
  const products = api.product.getAll.useQuery(
    { id },
    { refetchOnWindowFocus: false }
  )

  const orders = api.order.getOrders.useQuery(
    { userId: id },
    { refetchOnWindowFocus: false }
  )

  const deleteProductMutation = api.product.delete.useMutation()

  const deleteProduct = (id: string) => {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      confirmButtonColor: '#a02219',
    }).then(result => {
      if (result.isConfirmed) {
        deleteProductMutation.mutateAsync({ id })
      }
    })
  }

  const router = useRouter()

  useEffect(() => {
    const productId = deleteProductMutation.variables?.id

    if (deleteProductMutation.isError) {
      Swal.fire({
        title: 'An error ocurred',
        icon: 'error',
        text: deleteProductMutation.error.message,
        showCancelButton: true,
        confirmButtonText: 'Try again',
        confirmButtonColor: '#a02219',
      }).then(result => {
        if (result.isConfirmed && productId) {
          deleteProduct(productId)
        }
      })
      return
    }

    if (deleteProductMutation.isSuccess) {
      Swal.fire({
        title: 'Product deleted successfully',
        icon: 'success',
        html: `A product with the id <b>${productId}</b> was removed!`,
        showCancelButton: true,
        confirmButtonText: 'Reload',
        confirmButtonColor: '#a02219',
      }).then(result => {
        if (result.isConfirmed) {
          router.reload()
        }
      })

      return
    }
  }, [deleteProductMutation.isSuccess, deleteProductMutation.isError])

  return (
    <MainLayout title={name}>
      <header className="flex items-center justify-center gap-4 p-6">
        <Avatar src={image} alt="Profile image" size={200} />
        <div className="flex flex-col gap-2">
          <h2
            className={clsx([
              'text-2xl',
              'font-semibold',
              'sm:text-3xl',
              'md:text-4xl',
              'lg:text-5xl',
              'xl:text-6xl',
              'line-clamp-2',
            ])}>
            {name}
          </h2>
          <h3
            className={clsx([
              'flex w-full items-center gap-1 truncate text-xl font-light text-label',
              'flex',
              'w-full',
              'items-center',
              'gap-1',
              'truncate',
              'font-light',
              'text-label',
              'text-sm',
              'sm:text-base',
              'md:text-lg',
            ])}
            aria-label="User ID">
            <MdPerson className="shrink-0" />
            {id}
          </h3>
        </div>
      </header>

      <Divider className="mt-0" />

      <main>
        {orders.isLoading ? (
          <section className="grid place-items-center gap-2 text-primary">
            <RadialProgress
              value={70}
              className="animate-spin"
              size="4rem"
              thickness="7px"
            />
            <span className="font-bold">Loading orders</span>
          </section>
        ) : null}

        {!orders.isError && !orders.isLoading ? (
          <>
            <SectionHeader title="In process" id="purchased" />

            <section className="scroll-section">
              {orders.data?.length === 0 ? (
                <SectionNoContent section="Orders" />
              ) : null}

              {orders.data &&
                orders.data.map((order, i) => (
                  <PurchasedProductCard
                    // eslint-disable-next-line react/no-array-index-key
                    key={i}
                    id={order.id}
                    image="https://assets.adidas.com/images/w_600,f_auto,q_auto/28530d07245942fc944dae680084fb30_9366/Balon_Al_Rihla_Pro_Blanco_H57783_01_standard.jpg"
                    status={order.status}
                  />
                ))}
            </section>
          </>
        ) : null}

        <SectionHeader title="Products" id="products" />
        <section className="scroll-section">
          {products.isLoading
            ? [0, 1, 2, 3, 4].map(item => <ProductCardLoading key={item} />)
            : null}

          {products.data?.length === 0 ? (
            <SectionNoContent section="Products" />
          ) : null}

          {products.isError ? (
            <SectionFetchError error={products.error.message} />
          ) : null}

          {products.data &&
            products.data.map(product => (
              <article
                className="flex flex-col items-end gap-2"
                key={product.id}>
                <ProductCard
                  id={product.id}
                  image={product.image}
                  category={product.category}
                  title={product.title}
                  description={product.description}
                  price={product.price}
                  owner={product.owner.image}
                />
                <Button
                  color="ghost"
                  size="sm"
                  aria-label="Delete product"
                  onClick={() => deleteProduct(product.id)}>
                  <MdDelete size={20} className="text-error" />
                </Button>
              </article>
            ))}
        </section>
      </main>
    </MainLayout>
  )
}

export default Page
