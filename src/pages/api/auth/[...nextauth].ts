import NextAuth, { type NextAuthOptions } from 'next-auth'
import { PrismaAdapter } from '@next-auth/prisma-adapter'
import GoogleProvider from 'next-auth/providers/google'
import FacebookProvider from 'next-auth/providers/facebook'

import EmailProvider from 'next-auth/providers/email'
import { prisma } from '~/server/db'
import { env } from '~/env/server.mjs'

const adapter = PrismaAdapter(prisma)
adapter.createUser = async ({ name, ...user }) => {
  let newName = name ?? ''

  if (!name) {
    const count = await prisma.user.count()
    newName = `User${count + 1}`
  }

  return prisma.user
    .create({
      data: {
        ...user,
        name: newName,
      },
      select: null,
    })
    .then()
}

export const authOptions: NextAuthOptions = {
  // Include user.id on session
  callbacks: {
    async session({ session, user }) {
      const profile = await prisma.user.findFirst({
        where: { id: user.id },
      })

      if (profile) {
        session.user = profile
      }

      return session
    },
    redirect: ({ baseUrl }) => {
      return baseUrl
    },
  },
  pages: {
    signIn: '/auth/signin',
    signOut: '/auth/signout',
    verifyRequest: '/auth/verify-email',
    error: '/auth/error',
    newUser: '/',
  },
  // Configure one or more authentication providers
  adapter,
  providers: [
    EmailProvider({
      server: {
        host: env.EMAIL_SERVER_HOST,
        port: env.EMAIL_SERVER_PORT,
        auth: {
          user: env.EMAIL_SERVER_USER,
          pass: env.EMAIL_SERVER_PASSWORD,
        },
      },
      from: env.EMAIL_FROM,
    }),
    GoogleProvider({
      clientId: env.GOOGLE_CLIENT_ID,
      clientSecret: env.GOOGLE_CLIENT_SECRET,
    }),
    FacebookProvider({
      clientId: env.FACEBOOK_CLIENT_ID,
      clientSecret: env.FACEBOOK_CLIENT_SECRET,
    }),
  ],
}

export default NextAuth(authOptions)
