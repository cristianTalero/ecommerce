import clsx from 'clsx'
import type { NextPage, NextPageContext } from 'next'
import { useSession } from 'next-auth/react'
import { Button, Card, Divider } from 'react-daisyui'
import { BiDollar, BiPhone } from 'react-icons/bi'
import { BsX } from 'react-icons/bs'
import { MdMap, MdPerson } from 'react-icons/md'
import { TbNumber2 } from 'react-icons/tb'
import { CustomImage } from '~/components/utils/CustomImage'
import { ShowMoreText } from '~/components/utils/ShowMoreText'
import { MainLayout } from '~/layouts/MainLayout'
import { statuses } from '~/utils/products'
import { toCurrency } from '~/utils/toCurrency'
import { prisma } from '~/server/db'
import { getServerAuthSession } from '~/server/auth'
import type { Order } from '@prisma/client'
import { useEffect, useMemo } from 'react'
import moment from 'moment'
import { api } from '~/utils/api'
import Swal from 'sweetalert2'
import { useRouter } from 'next/router'
import Link from 'next/link'

export async function getServerSideProps(ctx: NextPageContext) {
  const id = ctx.query.id as string
  const session = await getServerAuthSession(ctx as never)

  if (!session?.user) {
    return {
      redirect: {
        destination: '/auth/signin',
        permanent: false,
      },
    }
  }

  const order = await prisma.order.findFirst({
    where: { id },
    include: {
      product: {
        select: {
          image: true,
          category: true,
          title: true,
          description: true,
        },
      },
    },
  })

  if (!order) {
    return {
      notFound: true,
    }
  }

  if (order.userId !== session.user.id) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }

  return {
    props: order,
  }
}

type PageProps = {
  product: {
    image: string
    category: string
    title: string
    description: string
  }
} & Order

const Page: NextPage<PageProps> = ({
  id,
  name,
  address,
  phone,
  units,
  price,
  status,
  arrived,
  product,
  productId,
}) => {
  useSession({ required: true })

  const confirmOrderMutation = api.order.confirmOrder.useMutation()
  const cancelOrderMutation = api.order.cancelOrder.useMutation()

  const data = useMemo(
    () => [
      {
        label: name,
        'aria-label': 'Name',
        icon: <MdPerson />,
      },
      {
        label: address,
        'aria-label': 'Address',
        icon: <MdMap />,
      },
      {
        label: phone,
        'aria-label': 'Phone',
        icon: <BiPhone />,
      },
      {
        label: units,
        'aria-label': 'Units',
        icon: <TbNumber2 />,
      },
    ],
    [name, address, phone, units]
  )

  const statusData = statuses[status]

  const router = useRouter()

  useEffect(() => {
    if (status === 'actually' && arrived.getUTCDate() <= Date.now()) {
      confirmOrderMutation.mutate({
        id,
      })

      router.reload()
    }
  }, [])

  const cancelOrder = async () => {
    Swal.fire({
      title: 'Are you sure?',
      icon: 'question',
      text: 'You will cancel order',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      confirmButtonColor: '#a02219',
    }).then(result => {
      if (result.isConfirmed) {
        cancelOrderMutation.mutate({
          id,
        })
      }
    })
  }

  useEffect(() => {
    if (cancelOrderMutation.isError) {
      Swal.fire({
        title: 'An error ocurred',
        icon: 'error',
        text: cancelOrderMutation.error.message,
        showCancelButton: true,
        confirmButtonText: 'Try again',
        confirmButtonColor: '#a02219',
      }).then(result => {
        if (result.isConfirmed) {
          cancelOrder()
        }
      })
      return
    }

    if (cancelOrderMutation.isSuccess) {
      Swal.fire({
        title: 'Order canceled successfully',
        icon: 'success',
        html: `A order with the id <b>${id}</b> was canceled!`,
        showCancelButton: true,
        confirmButtonText: 'Reload',
        confirmButtonColor: '#a02219',
      }).then(result => {
        if (result.isConfirmed) {
          router.reload()
        }
      })

      return
    }
  }, [cancelOrderMutation.isSuccess, cancelOrderMutation.isError])

  return (
    <MainLayout title="Purchased">
      <main className="grid place-items-center p-4">
        <Card className="sized-card bg-base-100" bordered>
          <picture className="relative h-80 w-full lg:h-96">
            <CustomImage src={product.image} alt="Product image" fill />
          </picture>
          <Card.Body className="flex flex-col gap-4">
            <h3 className="flex items-center gap-2 text-lg font-bold capitalize text-secondary">
              {product.category}
            </h3>
            <Link href={`/products/${productId}`}>
              <h1 className="link-hover text-4xl font-black">
                {product.title}
              </h1>
            </Link>

            <ShowMoreText className="text-2xl" content={product.description} />

            <Divider className="m-0" />

            <div className="text-lg font-normal text-secondary">
              {data.map(item => (
                <h4
                  key={item['aria-label']}
                  className="flex items-start gap-2"
                  aria-label={item['aria-label']}>
                  {item.icon}
                  {item.label}
                </h4>
              ))}
              <h4 className="flex items-center gap-2">
                <BiDollar />
                {toCurrency(price)}
              </h4>
            </div>

            <Divider className="m-0" />
            <Card.Actions className="justify flex flex-col gap-6">
              <h2
                className={clsx([
                  'text-3xl',
                  'm-auto',
                  'font-bold',
                  'text-success',
                  'flex',
                  'items-center',
                  'gap-3',
                  statusData.color,
                ])}>
                {statusData.icon}
                {statusData.label}
                {status === 'actually' ? (
                  <span className="text-base font-normal">
                    ({moment(arrived).toNow()})
                  </span>
                ) : null}
              </h2>

              {status === 'actually' ? (
                <Button
                  fullWidth
                  type="button"
                  className="grow"
                  variant={'outline'}
                  onClick={cancelOrder}
                  startIcon={<BsX size={20} />}
                  color="primary">
                  Cancel order
                </Button>
              ) : null}
            </Card.Actions>
          </Card.Body>
        </Card>
      </main>
    </MainLayout>
  )
}

export default Page
