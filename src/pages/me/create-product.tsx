import { ErrorMessage } from '@hookform/error-message'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, FileInput, Form, Input, Textarea } from 'react-daisyui'
import { type FieldValues, useForm } from 'react-hook-form'
import { BsPlusLg } from 'react-icons/bs'
import { MainLayout } from '~/layouts/MainLayout'
import { ProductSchema } from '~/utils/schemas/product'
import { api } from '~/utils/api'
import { Suspense, useEffect, useRef } from 'react'
import { toBase64 } from '~/utils/toBase64'
import dynamic from 'next/dynamic'
import Swal from 'sweetalert2'
import { useRouter } from 'next/router'
import { keys as categories } from '~/utils/products'
import { useSession } from 'next-auth/react'

const AsyncLoading = dynamic(async () => {
  const asyncLoading = await import('~/components/utils/AsyncLoading')
  return asyncLoading.AsyncLoading
})

export default function Page() {
  useSession({ required: true })
  const router = useRouter()

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(ProductSchema),
    defaultValues: {
      title: '',
      description: '',
      price: 20000,
      category: 'default',
      max_units: 0,
    },
  })

  const createProductMutation = api.product.create.useMutation()
  const fileRef = useRef<HTMLInputElement>(null)

  const onSubmit = async (e: FieldValues) => {
    const image = fileRef.current?.files?.item(0)

    if (image) {
      const base64 = (await toBase64(image)) as string

      await createProductMutation.mutateAsync({
        title: e.title,
        description: e.description,
        image: base64,
        price: e.price,
        category: e.category,
        max_units: e.max_units,
      })
    }
  }

  useEffect(() => {
    if (createProductMutation.isError) {
      Swal.fire({
        title: 'An error ocurred',
        icon: 'error',
        text: createProductMutation.error.message,
        showCancelButton: true,
        confirmButtonText: 'Try again',
        confirmButtonColor: '#a02219',
      }).then(result => {
        const data = createProductMutation.variables

        if (result.isConfirmed && data) {
          onSubmit(data)
        }
      })
      return
    }

    if (createProductMutation.isSuccess) {
      const id = createProductMutation.data.id

      Swal.fire({
        title: 'Product created successfully',
        icon: 'success',
        html: `A product with the id <b>${id}</b> was created!`,
        showCancelButton: true,
        confirmButtonText: 'Show',
        confirmButtonColor: '#a02219',
      }).then(result => {
        if (result.isConfirmed) {
          router.push(`/products/${id}`)
        }
      })

      return
    }
  }, [createProductMutation.isSuccess, createProductMutation.isError])

  return (
    <MainLayout title="New product">
      {createProductMutation.isLoading ? (
        <Suspense>
          <AsyncLoading label="Creating product" />
        </Suspense>
      ) : null}

      <main className="hero">
        <Form
          className="sized-card rounded-box flex w-screen flex-col gap-3 border bg-base-100 p-6"
          onSubmit={handleSubmit(onSubmit)}>
          <h1 className="mb-2 text-xl font-black">New product</h1>

          <div className="form-control w-full">
            <Form.Label htmlFor="title" title="Title" />
            <Input
              id="title"
              placeholder="A great product"
              bordered
              {...register('title')}
            />
            <ErrorMessage
              as={<span className="text-xs font-bold text-error" />}
              errors={errors}
              name="title"
            />
          </div>
          <div className="form-control w-full">
            <Form.Label htmlFor="description" title="Description" />
            <Textarea
              id="description"
              placeholder="A great description"
              className="w-full resize-none"
              bordered
              {...register('description')}
            />
            <ErrorMessage
              as={<span className="text-xs font-bold text-error" />}
              errors={errors}
              name="description"
            />
          </div>
          <div className="form-control w-full">
            <Form.Label htmlFor="image" title="Image" />
            <FileInput
              required
              ref={fileRef}
              id="image"
              bordered
              accept="image/webp, image/png, image/jpeg"
            />
          </div>
          <div className="form-control w-full">
            <Form.Label htmlFor="price" title="Price" />
            <Input
              id="price"
              type="number"
              bordered
              {...register('price', { valueAsNumber: true })}
            />
            <ErrorMessage
              as={<span className="text-xs font-bold text-error" />}
              errors={errors}
              name="price"
            />
          </div>
          <div className="form-control w-full">
            <Form.Label htmlFor="category" title="Category" />
            <select
              className="select-bordered select capitalize"
              id="category"
              {...register('category')}>
              <option disabled value="default">
                Select the category
              </option>
              {categories.map(category => (
                <option key={category}>{category}</option>
              ))}
            </select>
            <ErrorMessage
              as={<span className="text-xs font-bold text-error" />}
              errors={errors}
              name="category"
            />
          </div>
          <div className="form-control w-full">
            <Form.Label htmlFor="max-units" title="Max units">
              0 is no max
            </Form.Label>
            <Input
              id="max-units"
              type="number"
              bordered
              {...register('max_units', { valueAsNumber: true })}
            />
            <ErrorMessage
              as={<span className="text-xs font-bold text-error" />}
              errors={errors}
              name="max_units"
            />
          </div>

          <Button
            fullWidth
            type="submit"
            className="mt-4"
            startIcon={<BsPlusLg />}>
            Create
          </Button>
        </Form>
      </main>
    </MainLayout>
  )
}
