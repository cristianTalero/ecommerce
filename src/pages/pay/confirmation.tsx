import clsx from 'clsx'
import type { NextPageContext, NextPage } from 'next'
import { useSession } from 'next-auth/react'
import Link from 'next/link'

import { FaShoppingBag } from 'react-icons/fa'
import { MdCheckCircle, MdError } from 'react-icons/md'

import { PayLayout } from '~/layouts/PayLayout'

export function getServerSideProps(ctx: NextPageContext) {
  const error = ctx.query.error

  if (!error) {
    return { props: { error: false } }
  }

  return { props: { error } }
}

type ConfirmationProps = {
  error: string | false
}

const Confirmation: NextPage<ConfirmationProps> = ({ error }) => {
  const { data: session, status } = useSession({ required: true })

  return (
    <PayLayout title="Confirmation" pay error={Boolean(error)}>
      <main className="grid h-full place-items-center">
        <section
          className={clsx([
            'text-[2.5rem]',
            'sm:text-[3rem]',
            'font-semibold',
            'my-[5rem]',
            'flex',
            'flex-col',
            'gap-5',
            'text-center',
            error ? 'text-error' : 'text-success',
          ])}>
          {!error ? (
            <>
              <MdCheckCircle size={200} className="m-auto" />
              Payment successfully!
            </>
          ) : (
            <>
              <MdError size={200} className="m-auto" />
              Payment failed!
              <span className="block text-center text-[2rem] font-normal">
                {error}
              </span>
            </>
          )}
          <Link
            href={`/users/${session?.user?.id}#purchased`}
            className={clsx([
              'btn',
              'btn-outline',
              'btn-lg',
              'flex',
              'gap-3',
              status === 'loading' ? 'loading' : '',
            ])}>
            <FaShoppingBag />
            See order
          </Link>
        </section>
      </main>
    </PayLayout>
  )
}

export default Confirmation
