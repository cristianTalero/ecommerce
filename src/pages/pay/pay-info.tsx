import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { type FormEvent, useState, useEffect, Suspense } from 'react'
import { Form, Card, Input, Button, Swap, Tooltip } from 'react-daisyui'
import { BsArrowLeft } from 'react-icons/bs'
import { MdCheckCircle, MdError } from 'react-icons/md'
import { PayLayout } from '~/layouts/PayLayout'
import { useRequiredCart } from '~/hooks/useRequiredCart'
import { api } from '~/utils/api'
import { BiLock } from 'react-icons/bi'
import dynamic from 'next/dynamic'
import { useCartStore } from '~/store/cartStore'

const AsyncLoading = dynamic(async () => {
  const asyncLoading = await import('~/components/utils/AsyncLoading')
  return asyncLoading.AsyncLoading
})

const PayInfo: NextPage = () => {
  useRequiredCart()
  const payMutation = api.order.pay.useMutation()

  const [card, setCard] = useState(false)
  const router = useRouter()

  const { products, amount } = useCartStore(cart => ({
    products: cart.items,
    amount: cart.computed.total,
  }))

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault()
    const data = localStorage.getItem('personalInformation')

    if (!data) {
      router.replace('/pay/personal-information')
      return
    }

    const personalInformation = JSON.parse(data)

    payMutation.mutateAsync({
      name: personalInformation.name,
      address: personalInformation.address,
      phone: personalInformation.phone,
      amount,
      declined: !card,
      products: products.map(product => ({
        id: product.id,
        units: product.units,
        price: product.price,
      })),
    })
  }

  const { clearCart } = useCartStore(cart => ({
    clearCart: cart.removeAll,
  }))

  useEffect(() => {
    if (payMutation.isError) {
      router.replace(`/pay/confirmation?error=${payMutation.error.message}`)
      return
    }

    if (payMutation.isSuccess) {
      router
        .replace('/pay/confirmation')
        .then(res => (res ? clearCart() : null))
      return
    }
  }, [payMutation.isError, payMutation.isSuccess])

  const isLoading =
    payMutation.isLoading || payMutation.isError || payMutation.isSuccess

  return (
    <PayLayout title="Pay information" pay>
      {isLoading ? (
        <Suspense>
          <AsyncLoading />
        </Suspense>
      ) : null}

      <main
        onSubmit={onSubmit}
        className="my-4 grid h-full w-full place-items-center">
        <Form className="sized-card card-bordered card card-body bg-base-100">
          <Card.Title className="flex justify-between">
            Pay information
            <Tooltip
              message={card ? 'Failed card' : 'Functional card'}
              position="left">
              <Swap
                flip
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                onChange={e => setCard((e.target as any).checked)}
                offElement={
                  <MdCheckCircle size={25} className="text-success" />
                }
                onElement={<MdError size={25} className="text-error" />}
              />
            </Tooltip>
          </Card.Title>
          <div className="form-control">
            <Form.Label htmlFor="name" title="Your name" />
            <Input
              id="name"
              placeholder="John Doe"
              value="John Doe"
              disabled
              aria-disabled
            />
          </div>
          <div className="form-control">
            <Form.Label htmlFor="card" title="Card" />
            <Input
              id="card"
              value={card ? 4000056655665556 : 4000000000009995}
              placeholder="1234 5678 91011 1213"
              disabled
              aria-disabled
            />
          </div>
          <div className="form-control">
            <Form.Label htmlFor="expiration-month" title="Expiration month" />
            <Input
              id="expiration-month"
              type="number"
              placeholder="8"
              value={7}
              disabled
              aria-disabled
            />
          </div>
          <div className="form-control">
            <Form.Label htmlFor="expiration-year" title="Expiration year" />
            <Input
              id="expiration-year"
              type="number"
              placeholder="2024"
              value={new Date().getFullYear() + 2}
              disabled
              aria-disabled
            />
          </div>
          <div className="form-control">
            <Form.Label htmlFor="security-code" title="Security code (CVC)" />
            <Input
              id="security-code"
              type="number"
              placeholder="123"
              value={753}
              disabled
              aria-disabled
            />
          </div>
          <Button
            type="submit"
            className="mt-3"
            startIcon={<BiLock />}
            loading={isLoading}>
            Pay
          </Button>
          <Button
            type="button"
            variant="outline"
            startIcon={<BsArrowLeft />}
            loading={isLoading}
            onClick={router.back}>
            Prev
          </Button>
        </Form>
      </main>
    </PayLayout>
  )
}

export default PayInfo
