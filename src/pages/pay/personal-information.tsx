import { ErrorMessage } from '@hookform/error-message'
import { zodResolver } from '@hookform/resolvers/zod'
import { type NextPage } from 'next'

import { useRouter } from 'next/router'
import { useState } from 'react'
import { Button, Card, Form, Input } from 'react-daisyui'
import { type FieldValues, useForm } from 'react-hook-form'
import { BsArrowRight } from 'react-icons/bs'
import { z } from 'zod'
import { useRequiredCart } from '~/hooks/useRequiredCart'
import { PayLayout } from '~/layouts/PayLayout'
import { trim } from '~/utils/trim'

const informationSchema = z.object({
  name: z.string().transform(trim).pipe(z.string().min(1, "Name can't empty!")),
  address: z
    .string()
    .transform(trim)
    .pipe(z.string().min(1, "Address can't empty!")),
  phone: z
    .string()
    .transform(trim)
    .pipe(
      z
        .string()
        .min(1, "Phone can't empty!")
        .regex(/^\+[0-9]{2,3}[0-9]{10}/, "It's not a number!")
    ),
})

const PersonalInformation: NextPage = () => {
  useRequiredCart()

  const [loading, setLoading] = useState(false)

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(informationSchema),
  })

  const router = useRouter()

  const onSubmit = (e: FieldValues) => {
    setLoading(true)
    localStorage.setItem('personalInformation', JSON.stringify(e))
    router.push('/pay/pay-info')
  }

  return (
    <PayLayout title="Personal information" pay={false}>
      <main className="my-4 grid h-full w-full place-items-center">
        <Form
          onSubmit={handleSubmit(onSubmit)}
          className="sized-card card-bordered card card-body bg-base-100">
          <Card.Title>Personal information</Card.Title>
          <div className="form-control">
            <Form.Label htmlFor="name" title="Your name" />
            <Input id="name" placeholder="John Doe" {...register('name')} />
            <ErrorMessage
              as={<span className="text-xs font-bold text-error" />}
              errors={errors}
              name="name"
            />
          </div>
          <div className="form-control">
            <Form.Label htmlFor="address" title="Address" />
            <Input
              id="address"
              placeholder="Cr 35b #45 - 02"
              {...register('address')}
            />
            <ErrorMessage
              as={<span className="text-xs font-bold text-error" />}
              errors={errors}
              name="address"
            />
          </div>
          <div className="form-control">
            <Form.Label htmlFor="phone" title="Phone" />
            <Input
              id="phone"
              type="tel"
              placeholder="+573186393214"
              {...register('phone')}
            />
            <ErrorMessage
              as={<span className="text-xs font-bold text-error" />}
              errors={errors}
              name="phone"
            />
          </div>
          <Button
            type="submit"
            className="mt-3"
            endIcon={<BsArrowRight />}
            loading={loading}>
            Next
          </Button>
        </Form>
      </main>
    </PayLayout>
  )
}

export default PersonalInformation
