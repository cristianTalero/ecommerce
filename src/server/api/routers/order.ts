import { TRPCError } from '@trpc/server'
import { z } from 'zod'
import { client } from '~/utils/stripe'

import { trim } from '~/utils/trim'
import { createTRPCRouter, protectedProcedure } from '../trpc'

export const orderRouter = createTRPCRouter({
  getOrders: protectedProcedure
    .input(
      z
        .object({
          userId: z
            .string()
            .transform(trim)
            .pipe(z.string().min(1, "User ID can't be empty!")),
        })
        .required()
        .strict()
    )
    .query(async ({ input, ctx }) => {
      const userId = input.userId

      if (userId !== ctx.session.user.id) {
        throw new TRPCError({
          message: 'User is not the owner!',
          code: 'FORBIDDEN',
        })
      }

      const orders = await ctx.prisma.order.findMany({
        where: { userId },
        select: { id: true, status: true },
        orderBy: [
          {
            createdAt: 'desc',
          },
        ],
      })

      return orders
    }),
  pay: protectedProcedure
    .input(
      z
        .object({
          name: z
            .string()
            .transform(trim)
            .pipe(z.string().min(1, "Name can't be empty!")),
          address: z
            .string()
            .transform(trim)
            .pipe(z.string().min(1, "Address can't be empty!")),
          phone: z
            .string()
            .transform(trim)
            .pipe(z.string().min(1, "Phone can't be empty!")),
          amount: z.number(),
          declined: z.boolean(),
          products: z
            .array(
              z.object({
                id: z
                  .string()
                  .pipe(z.string().min(1, "Product ID can't be empty!")),
                units: z.number().min(1),
                price: z.number(),
              })
            )
            .min(1, 'One product almost!'),
        })
        .required()
        .strict()
    )
    .mutation(async ({ input, ctx }) => {
      const payment = await client.paymentIntents.create({
        amount: input.amount,
        currency: 'gbp',
        payment_method: !input.declined
          ? 'pm_card_visa_chargeDeclined'
          : 'pm_card_visa_debit',
      })

      const paymentId = payment.id

      await ctx.prisma.$transaction(
        input.products.map(product => {
          return ctx.prisma.order.create({
            data: {
              name: input.name,
              address: input.address,
              phone: input.phone,
              orderId: paymentId,
              status: input.declined ? 'actually' : 'cancel',
              userId: ctx.session.user.id,
              price: product.price * product.units,
              units: product.units,
              arrived: new Date(Date.now() + 86400000),
              productId: product.id,
            },
            select: { id: true },
          })
        })
      )

      await client.paymentIntents.confirm(paymentId)
    }),
  confirmOrder: protectedProcedure
    .input(
      z.object({
        id: z
          .string()
          .transform(trim)
          .pipe(z.string().min(1, "ID can't be empty!")),
      })
    )
    .mutation(async ({ input, ctx }) => {
      await ctx.prisma.order.update({
        where: { id: input.id },
        data: {
          status: 'success',
        },
        select: null,
      })
    }),
  cancelOrder: protectedProcedure
    .input(
      z.object({
        id: z
          .string()
          .transform(trim)
          .pipe(z.string().min(1, "ID can't be empty!")),
      })
    )
    .mutation(async ({ input, ctx }) => {
      await ctx.prisma.order.update({
        where: { id: input.id },
        data: {
          status: 'cancel',
        },
        select: null,
      })
    }),
})
