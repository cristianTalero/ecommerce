import { z } from 'zod'
import { createTRPCRouter, protectedProcedure, publicProcedure } from '../trpc'
import { TRPCError } from '@trpc/server'
import { trim } from '~/utils/trim'

export const reviewRouter = createTRPCRouter({
  getAll: publicProcedure
    .input(
      z
        .object({
          productId: z.string(),
        })
        .required()
        .strict()
    )
    .query(async ({ input, ctx }) => {
      const reviews = await ctx.prisma.review.findMany({
        where: { productId: input.productId },
        include: { owner: { select: { id: true, name: true, image: true } } },
        orderBy: [
          {
            createdAt: 'desc',
          },
        ],
      })

      return reviews
    }),
  upsert: protectedProcedure
    .input(
      z
        .object({
          message: z
            .string()
            .transform(trim)
            .pipe(z.string().min(1, "Message can't be empty!")),
          rate: z.number().min(1, 'Min one rate!').max(5, 'Max 5 rates!'),
          productId: z
            .string()
            .transform(trim)
            .pipe(z.string().min(1, "Product ID can't be empty!")),
        })
        .required()
        .strict()
    )
    .mutation(async ({ input, ctx }) => {
      try {
        const userId = ctx.session.user.id

        await ctx.prisma.review.upsert({
          where: {
            productId_ownerId: {
              ownerId: userId,
              productId: input.productId,
            },
          },
          create: {
            message: input.message,
            rate: input.rate,
            ownerId: userId,
            productId: input.productId,
          },
          update: {
            message: input.message,
            rate: input.rate,
          },
        })
      } catch (err) {
        throw new TRPCError({
          message: (err as Error).message,
          code: 'INTERNAL_SERVER_ERROR',
          cause: err,
        })
      }
    }),
  delete: protectedProcedure
    .input(z.object({ id: z.string() }))
    .mutation(async ({ input, ctx }) => {
      try {
        await ctx.prisma.review.delete({
          where: { id: input.id },
          select: null,
        })
      } catch (err) {
        throw new TRPCError({
          message: (err as Error).message,
          code: 'INTERNAL_SERVER_ERROR',
          cause: err,
        })
      }
    }),
})
