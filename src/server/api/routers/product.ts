import { z } from 'zod'
import { client } from '~/utils/cloudinary'
import { ProductSchema } from '~/utils/schemas/product'
import { createTRPCRouter, protectedProcedure, publicProcedure } from '../trpc'
import { TRPCError } from '@trpc/server'

export const productRouter = createTRPCRouter({
  search: publicProcedure
    .input(
      z.object({
        value: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const products = await ctx.prisma.product.findMany({
        where: {
          OR: [
            {
              title: {
                contains: input.value,
              },
            },
            {
              description: {
                contains: input.value,
              },
            },
          ],
        },
        select: {
          image: true,
          title: true,
          category: true,
          price: true,
          id: true,
          owner: {
            select: {
              image: true,
            },
          },
        },
        take: 100,
      })

      return products
    }),
  getAll: publicProcedure
    .input(z.object({ id: z.string() }))
    .query(async ({ input, ctx }) => {
      try {
        const products = await ctx.prisma.product.findMany({
          where: { ownerId: input.id },
          include: { owner: { select: { image: true } } },
        })

        return products
      } catch (err) {
        throw new TRPCError({
          message: (err as Error).message,
          code: 'INTERNAL_SERVER_ERROR',
          cause: err,
        })
      }
    }),
  getByCategory: publicProcedure
    .input(z.object({ category: z.string().nullable() }))
    .query(async ({ input, ctx }) => {
      try {
        const products = await ctx.prisma.product.findMany({
          where: input.category
            ? { category: input.category as never }
            : undefined,
          include: { owner: { select: { image: true } } },
        })

        return products
      } catch (err) {
        throw new TRPCError({
          message: (err as Error).message,
          code: 'INTERNAL_SERVER_ERROR',
          cause: err,
        })
      }
    }),
  create: protectedProcedure
    .input(ProductSchema.merge(z.object({ image: z.string() })))
    .output(z.object({ id: z.string() }))
    .mutation(async ({ input, ctx }) => {
      try {
        const id = ctx.session.user.id

        const uploaded = await client.uploader.upload_large(input.image, {
          format: 'webp',
          folder: 'ecommerce',
          unique_filename: true,
          transformation: {
            quality: 80,
          },
        })

        const product = await ctx.prisma.product.create({
          data: {
            ...input,
            image: uploaded.secure_url,
            ownerId: id,
          },
          select: { id: true },
        })

        return { id: product.id }
      } catch (err) {
        throw new TRPCError({
          message: (err as Error).message,
          code: 'INTERNAL_SERVER_ERROR',
          cause: err,
        })
      }
    }),
  delete: protectedProcedure
    .input(z.object({ id: z.string() }))
    .mutation(async ({ input, ctx }) => {
      try {
        const product = await ctx.prisma.product.delete({
          where: { id: input.id },
          select: { image: true },
        })

        const imageId = product.image.split('.webp')[0]?.split('/')[8]

        await client.uploader.destroy(`ecommerce/${imageId}`)
      } catch (err) {
        throw new TRPCError({
          message: (err as Error).message,
          code: 'INTERNAL_SERVER_ERROR',
          cause: err,
        })
      }
    }),
})
