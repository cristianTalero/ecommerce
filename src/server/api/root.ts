import { createTRPCRouter } from './trpc'
import { orderRouter } from './routers/order'
import { productRouter } from './routers/product'
import { reviewRouter } from './routers/review'

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here
 */
export const appRouter = createTRPCRouter({
  order: orderRouter,
  product: productRouter,
  reviews: reviewRouter,
})

// export type definition of API
export type AppRouter = typeof appRouter
