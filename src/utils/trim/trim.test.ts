import { trim } from './'

let value = '     Test       value      '
const resultValue = 'Test value'

const trimMock = jest
  .spyOn(String.prototype, 'trim')
  .mockReturnValue('Test       value')

const replaceMock = jest
  .spyOn(String.prototype, 'replace')
  .mockReturnValueOnce('Test<><><><><><><>value')
  .mockReturnValueOnce('Test<>value')
  .mockReturnValueOnce(resultValue)

beforeAll(() => jest.clearAllMocks())

it('should trim a string value', () => {
  value = 'Test value      '

  expect(trimMock).toBeDefined()
  expect(replaceMock).toBeDefined()
  expect(trim(value)).toEqual(resultValue)
  expect(trimMock).toHaveBeenCalledTimes(1)
  expect(replaceMock).toHaveBeenCalledTimes(3)
})
