import { AiFillHome } from 'react-icons/ai'
import { BiFootball } from 'react-icons/bi'
import { BsDisplayFill } from 'react-icons/bs'
import { FaTruck, FaTshirt } from 'react-icons/fa'
import { MdCheckCircle, MdError, MdSmartToy } from 'react-icons/md'

export const categories = {
  clothes: <FaTshirt />,
  technology: <BsDisplayFill />,
  sports: <BiFootball />,
  home: <AiFillHome />,
  toys: <MdSmartToy />,
}

export type Category = keyof typeof categories

export const keys = Object.keys(categories)

export type Status = 'success' | 'actually' | 'cancel'

export const statuses = {
  success: {
    icon: <MdCheckCircle />,
    label: 'Received',
    color: 'text-success',
  },
  actually: {
    icon: <FaTruck />,
    label: 'In progress',
    color: 'text-warning',
  },
  cancel: {
    icon: <MdError />,
    label: 'Canceled',
    color: 'text-error',
  },
}
