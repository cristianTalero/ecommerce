import { v2 as cloudinary } from 'cloudinary'
import { env } from '~/env/server.mjs'

cloudinary.config({
  cloud_name: env.CLOUDINARY_CLOUD,
  api_key: env.CLOUDINARY_KEY,
  api_secret: env.CLOUDINARY_SECRET,
  secure: true,
})

export { cloudinary as client }
