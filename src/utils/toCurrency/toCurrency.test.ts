import { toCurrency } from './'

const value = 233500
const resultValue = 'COP 233,500'

const toLocaleStringMock = jest
  .spyOn(Number.prototype, 'toLocaleString')
  .mockReturnValue(resultValue)

afterAll(() => jest.clearAllMocks())

it('should parse number as COP currency', () => {
  expect(toLocaleStringMock).toBeDefined()
  expect(toCurrency(value)).toEqual(resultValue)
  expect(toLocaleStringMock).toHaveBeenCalledTimes(1)
})
