export function toCurrency(value: number) {
  const currency = value.toLocaleString('en-US', {
    style: 'currency',
    currency: 'COP',
    maximumFractionDigits: 0,
  })

  return currency
}
