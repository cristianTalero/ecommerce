import { z } from 'zod'
import { trim } from '../trim'

export const ProductSchema = z
  .object({
    title: z
      .string()
      .transform(trim)
      .pipe(z.string().min(1, "Title can't be empty!")),
    description: z
      .string()
      .transform(trim)
      .pipe(z.string().min(1, "Description can't be empty!")),
    price: z
      .number({ invalid_type_error: 'Price is required!' })
      .gt(0, 'Price can be greater than 0!'),
    category: z.enum(['clothes', 'technology', 'sports', 'home', 'toys'], {
      errorMap: () => ({ message: 'Invalid category!' }),
    }),
    max_units: z
      .number({ invalid_type_error: 'Price is required!' })
      .gte(0, "Max units can't be less than 0!")
      .default(0),
  })
  .required()
  .strict()
