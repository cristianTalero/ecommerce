import { toBase64 } from './'

describe('toBase64', () => {
  const fileMock = new File(['file content'], 'filename.txt', {
    type: 'text/plain',
  })
  const fileReaderMock = new FileReader()

  const base64 = 'data:text/plain;base64,ZmlsZSBjb250ZW50'

  const readAsDataURLSpy = jest.spyOn(fileReaderMock, 'readAsDataURL')
  const onloadend = jest.fn()
  const onerror = jest.fn()

  fileReaderMock.onloadend = onloadend
  fileReaderMock.onerror = onerror

  jest.spyOn(window, 'FileReader').mockImplementation(() => fileReaderMock)

  afterEach(() => jest.clearAllMocks())

  it('should read file and resolve with base64', async () => {
    const result = await toBase64(fileMock)
    expect(readAsDataURLSpy).toHaveBeenCalledWith(fileMock)
    expect(result).toEqual(base64)
    expect(onerror).not.toHaveBeenCalled()
  })

  it('should reject if the file cannot be read', async () => {
    const file = {} as File
    await expect(toBase64(file)).rejects.toEqual(
      new TypeError(
        "Failed to execute 'readAsDataURL' on 'FileReader': parameter 1 is not of type 'Blob'."
      )
    )
  })
})
