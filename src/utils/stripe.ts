import Stripe from 'stripe'
import { env } from '~/env/server.mjs'

let stripe: Stripe

function getInstance() {
  if (!stripe) {
    stripe = new Stripe(env.STRIPE_SECRET, {
      apiVersion: '2022-11-15',
      typescript: true,
    })
  }

  return stripe
}

export const client = getInstance()
