import { useRouter } from 'next/router'
import { Button } from 'react-daisyui'
import { IoReload } from 'react-icons/io5'
import { MdError } from 'react-icons/md'

type SectionFetchErrorProps = {
  error: string
}

export function SectionFetchError({ error }: SectionFetchErrorProps) {
  const router = useRouter()

  return (
    <article className="m-auto grid place-items-center">
      <MdError size={170} className="text-error" />
      <span className="text-4xl">An error ocurred!</span>
      <p className="mt-3 text-lg text-label">{error}</p>
      <Button
        startIcon={<IoReload />}
        variant="outline"
        className="mt-5"
        onClick={() => router.reload()}>
        Reload
      </Button>
    </article>
  )
}
