import { Divider } from 'react-daisyui'

type SectionHeaderProps = {
  title: string
  id: string
  filter?: boolean
}

export function SectionHeader({ title, id }: SectionHeaderProps) {
  return (
    <>
      <div className="relative flex justify-between px-4 py-2" id={id}>
        <h3 className="text-xl font-semibold capitalize">{title}</h3>
      </div>
      <Divider className="m-0" />
    </>
  )
}
