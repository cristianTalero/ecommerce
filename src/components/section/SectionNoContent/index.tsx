import { MdSentimentDissatisfied } from 'react-icons/md'

type SectionNoContentProps = {
  section: string
}

export function SectionNoContent({ section }: SectionNoContentProps) {
  return (
    <article className="m-auto grid place-items-center">
      <MdSentimentDissatisfied size={170} />
      <span className="text-4xl">There is no {section.toLowerCase()}</span>
    </article>
  )
}
