import { useRouter } from 'next/router'
import { type ReactNode, useEffect, useState } from 'react'
import { Button, ButtonGroup } from 'react-daisyui'
import { MdDelete, MdEdit } from 'react-icons/md'
import Swal from 'sweetalert2'
import { api } from '~/utils/api'
import { RatingForm } from '../RatingForm'

type RatingOwnReviewProps = {
  productId: string
  reviewId: string
  message: string
  rate: number
  children: ReactNode
}

export function RatingOwnReview({
  productId,
  reviewId,
  message,
  rate,
  children,
}: RatingOwnReviewProps) {
  const [update, setUpdate] = useState(false)

  const deleteReviewMutation = api.reviews.delete.useMutation()

  const deleteReview = (id: string) => {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      confirmButtonColor: '#a02219',
    }).then(result => {
      if (result.isConfirmed) {
        deleteReviewMutation.mutateAsync({ id })
      }
    })
  }

  const router = useRouter()

  useEffect(() => {
    const productId = deleteReviewMutation.variables?.id

    if (deleteReviewMutation.isError) {
      Swal.fire({
        title: 'An error ocurred',
        icon: 'error',
        text: deleteReviewMutation.error.message,
        showCancelButton: true,
        confirmButtonText: 'Try again',
        confirmButtonColor: '#a02219',
      }).then(result => {
        if (result.isConfirmed && productId) {
          deleteReview(productId)
        }
      })
      return
    }

    if (deleteReviewMutation.isSuccess) {
      Swal.fire({
        title: 'Review deleted successfully',
        icon: 'success',
        html: `A review with the id <b>${productId}</b> was removed!`,
        showCancelButton: true,
        confirmButtonText: 'Reload',
        confirmButtonColor: '#a02219',
      }).then(result => {
        if (result.isConfirmed) {
          router.reload()
        }
      })

      return
    }
  }, [deleteReviewMutation.isSuccess, deleteReviewMutation.isError])

  return (
    <>
      <article className="w-full self-start lg:w-1/2">
        <h3 className="mb-2 ml-2 font-semibold">Your review</h3>
        <div className="flex flex-col gap-2 lg:flex-row">
          {children}
          <ButtonGroup className="btn-group-horizontal self-end lg:self-start lg:btn-group-vertical">
            <Button
              color="ghost"
              size="sm"
              aria-label="Edit review"
              onClick={() => setUpdate(current => !current)}>
              <MdEdit size={20} className="text-info" />
            </Button>
            <Button
              color="ghost"
              size="sm"
              aria-label="Delete review"
              onClick={() => deleteReview(reviewId)}>
              <MdDelete size={20} className="text-error" />
            </Button>
          </ButtonGroup>
        </div>
      </article>

      {update ? (
        <RatingForm id={productId} message={message} stars={rate} />
      ) : null}
    </>
  )
}
