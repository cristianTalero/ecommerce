import { useState } from 'react'
import { Rating } from 'react-daisyui'

type RatingStarsProps = {
  value?: number
  stars: number
  disabled: boolean
  size?: 'sm' | 'md'
  onChange?: (_: number) => void
}

export function RatingStars({
  value,
  stars,
  disabled,
  size = 'sm',
  onChange,
}: RatingStarsProps) {
  const [star, setStar] = useState(value ?? 5)

  const starHandle = (e: number) => {
    setStar(e)
    onChange?.(e)
  }

  return (
    <Rating value={star} size={size} onChange={starHandle}>
      {Array.from<number>({ length: stars }).map((_, i) => (
        <Rating.Item
          // eslint-disable-next-line react/no-array-index-key
          key={i}
          className="mask mask-star-2"
          disabled={disabled}
          aria-disabled={disabled}
        />
      ))}
    </Rating>
  )
}
