import Link from 'next/link'
import moment from 'moment'
import { Avatar } from '~/components/utils/Avatar'
import { ShowMoreText } from '~/components/utils/ShowMoreText'
import { RatingStars } from '../RatingStars'
import type { Review } from '@prisma/client'

type RatingReviewProps = {
  owner: {
    id: string
    name: string
    image: string | null
  }
} & Omit<Review, 'id' | 'ownerId' | 'productId'>

export function RatingReview({
  message,
  rate,
  createdAt,
  updatedAt,
  owner,
}: RatingReviewProps) {
  const isUpdated = createdAt === updatedAt
  const userUrl = `/users/${owner.id}`

  return (
    <article className="rounded-box w-full grow border p-2 shadow">
      <div className="flex items-center gap-2">
        <Link href={userUrl} className="touchable shrink-0 self-start">
          <Avatar src={owner.image} alt="Profile avatar" size={40} />
        </Link>
        <div className="flex flex-col items-start gap-2">
          <Link href={userUrl} className="touchable font-bold">
            {owner.name}
          </Link>
          <RatingStars stars={rate} disabled />
          <ShowMoreText content={message} />
          <span className="text-sm font-semibold text-primary first-letter:uppercase">
            {moment(createdAt).fromNow()}{' '}
            {isUpdated ? `(Up. ${updatedAt})` : null}
          </span>
        </div>
      </div>
    </article>
  )
}
