export function RatingReviewLoading() {
  return (
    <article className="sk-100 rounded-box border p-2 shadow">
      <div className="flex gap-2">
        <div className="sk-200 h-10 w-10 shrink-0 self-start rounded-xl" />
        <div className="flex w-full flex-col gap-2">
          <div className="sk-200 h-3 w-32" />
          <div className="sk-200 h-3 w-24" />
          <div className="sk-200 h-14" />
          <div className="sk-200 h-4 w-20" />
        </div>
      </div>
    </article>
  )
}
