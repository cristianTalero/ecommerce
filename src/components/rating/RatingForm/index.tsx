import { useRouter } from 'next/router'
import { useEffect, useState, type FormEvent } from 'react'
import { Button, Card, Form, Textarea } from 'react-daisyui'
import Swal from 'sweetalert2'
import { api } from '~/utils/api'
import { trim } from '~/utils/trim'
import { RatingStars } from '../RatingStars'

type CreateRatingFormProps = {
  id: string
  message?: undefined
  stars?: undefined
}

type EditRatingFormProps = {
  id: string
  message: string
  stars: number
}

export function RatingForm({
  id,
  message: msg,
  stars,
}: CreateRatingFormProps | EditRatingFormProps) {
  const [message, setMessage] = useState(msg ?? '')
  const [rate, setRate] = useState(stars ?? 4)
  const [loading, setLoading] = useState(false)

  const createReviewMutation = api.reviews.upsert.useMutation()

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault()

    const parsedMessage = trim(message)

    if (parsedMessage) {
      setLoading(true)

      await createReviewMutation.mutateAsync({
        message: parsedMessage,
        rate,
        productId: id,
      })
    }
  }

  const router = useRouter()
  useEffect(() => {
    setLoading(false)

    if (createReviewMutation.isError) {
      Swal.fire({
        title: 'An error ocurred',
        icon: 'error',
        text: createReviewMutation.error.message,
      })
      return
    }

    if (createReviewMutation.isSuccess) {
      setMessage('')

      Swal.fire({
        title: 'Review created successfully',
        icon: 'success',
        text: 'Reload the page to see it',
        showCancelButton: true,
        confirmButtonText: 'Reload',
        confirmButtonColor: '#a02219',
      }).then(result => {
        if (result.isConfirmed) {
          router.reload()
        }
      })
    }
  }, [createReviewMutation.isError, createReviewMutation.isSuccess])

  return (
    <Card bordered className="sized-card bg-base-100">
      <Form className="card-body" onSubmit={onSubmit}>
        <Card.Title>Your review</Card.Title>
        <div className="form-control">
          <Form.Label title="Stars" className="font-semibold text-label" />
          <RatingStars
            value={stars}
            stars={5}
            disabled={false}
            size="md"
            onChange={e => setRate(e)}
          />
        </div>
        <div className="form-control">
          <Form.Label title="Message" className="font-semibold text-label" />
          <Textarea
            placeholder="A great choice!"
            borderOffset
            bordered
            className="resize-none"
            value={message}
            onChange={e => setMessage(e.currentTarget.value)}
          />
        </div>
        <Button type="submit" className="mt-3" loading={loading}>
          Post
        </Button>
      </Form>
    </Card>
  )
}
