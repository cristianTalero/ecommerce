export function RatingFormLoading() {
  return (
    <div className="sized-card sk-100 flex flex-col gap-4 p-8">
      <div className="sk-200 mb-2 h-6 w-32" />
      <div className="flex flex-col gap-2">
        <div className="sk-200 h-3 w-16" />
        <div className="sk-200 h-6 w-36" />
      </div>
      <div className="flex flex-col gap-2">
        <div className="sk-200 h-3 w-20" />
        <div className="sk-200 h-16" />
      </div>
      <div className="sk-200 h-10" />
    </div>
  )
}
