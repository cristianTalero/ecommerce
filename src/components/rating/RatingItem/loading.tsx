export function RatingItemLoading() {
  return (
    <div className="flex flex-col gap-4">
      <div className="flex justify-between">
        <div className="sk-200 h-5 w-16" />
        <div className="sk-200 h-5 w-16" />
      </div>
      <div className="sk-200 h-2 w-full rounded-full" />
    </div>
  )
}
