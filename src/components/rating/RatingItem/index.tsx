import { useMemo } from 'react'
import { Progress } from 'react-daisyui'
import { RatingStars } from '../RatingStars'

type RatingItemProps = {
  stars: number
  current: number
  total: number
}

export function RatingItem({ stars, current, total }: RatingItemProps) {
  const percentage = useMemo(() => {
    if (total > 0) {
      return (current * 100) / total
    }

    return 0
  }, [current, total])

  return (
    <div>
      <div className="flex justify-between">
        <RatingStars stars={stars} disabled />

        <h4 className="font-semibold">
          {current}{' '}
          <span className="font-light">
            {percentage > 0 ? `(${Math.floor(percentage)}%)` : null}
          </span>
        </h4>
      </div>
      <Progress className="progress-warning" value={percentage} max={100} />
    </div>
  )
}
