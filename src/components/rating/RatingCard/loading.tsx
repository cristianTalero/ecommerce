import { RatingItemLoading } from '../RatingItem/loading'

export function RatingCardLoading() {
  return (
    <article className="sized-card sk-100 p-4">
      <div className="sk-200 mb-6 h-6 w-32" />
      <div className="flex flex-col gap-3">
        <RatingItemLoading />
        <RatingItemLoading />
        <RatingItemLoading />
        <RatingItemLoading />
        <RatingItemLoading />
      </div>
    </article>
  )
}
