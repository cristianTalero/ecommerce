import { Card } from 'react-daisyui'
import { RatingItem } from '~/components/rating/RatingItem'

type RatingCardProps = {
  total: number
  total1: number
  total2: number
  total3: number
  total4: number
  total5: number
}

export function RatingCard({
  total,
  total1,
  total2,
  total3,
  total4,
  total5,
}: RatingCardProps) {
  return (
    <Card bordered className="sized-card bg-base-100 shadow">
      <Card.Body>
        <Card.Title className="mb-4">
          Rating <span className="font-light">({total})</span>
        </Card.Title>
        <RatingItem stars={5} current={total5} total={total} />
        <RatingItem stars={4} current={total4} total={total} />
        <RatingItem stars={3} current={total3} total={total} />
        <RatingItem stars={2} current={total2} total={total} />
        <RatingItem stars={1} current={total1} total={total} />
      </Card.Body>
    </Card>
  )
}
