import clsx from 'clsx'
import Link from 'next/link'
import { cloneElement } from 'react'
import { type Category, categories } from '~/utils/products'

type ProductCategoryCardProps = {
  category: Category
}

export function ProductCategoryCard({ category }: ProductCategoryCardProps) {
  return (
    <Link href={`/products/?category=${category}`}>
      <article
        className={clsx([
          'border-primary',
          'border-4',
          'h-36',
          'w-36',
          'grid',
          'place-items-center',
          'rounded-lg',
          'touchable',
          'shadow',
          'transition-all',
          'hover:shadow-primary',
          'text-primary',
          'p-3',
        ])}>
        {cloneElement(categories[category], {
          size: 40,
        })}
        <h3 className="text-xl font-semibold capitalize">{category}</h3>
      </article>
    </Link>
  )
}
