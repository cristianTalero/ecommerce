import clsx from 'clsx'
import Link from 'next/link'
import { Card } from 'react-daisyui'
import { CustomImage } from '~/components/utils/CustomImage'
import { type Status, statuses } from '~/utils/products'

type PurchasedProductCardProps = {
  id: string
  image: string
  status: Status
}

export function PurchasedProductCard({
  id,
  image,
  status,
}: PurchasedProductCardProps) {
  const statusData = statuses[status]

  return (
    <Link href={`/me/purchased/${id}`}>
      <Card className="touchable relative w-72 shrink-0 shadow">
        <figure className="relative h-40 w-full shadow">
          <CustomImage src={image} alt="Product image" fill />
        </figure>
        <Card.Actions
          className={clsx([
            'flex',
            'items-center',
            'justify-center',
            'p-3',
            statusData.color,
          ])}>
          {statusData.icon}
          <span className="font-bold">{statusData.label}</span>
        </Card.Actions>
      </Card>
    </Link>
  )
}
