export function ProductCardLoading() {
  return (
    <article className="sk-100 rounded-box w-72 shrink-0 shadow">
      <div className="sk-200 h-52" />
      <div className="flex flex-col gap-3 p-4">
        <div className="sk-200 h-4 w-16" />
        <div className="sk-200 h-6 w-28" />
        <div className="sk-200 h-16 w-full" />
        <div className="sk-200" />
      </div>
      <div className="flex items-center justify-around py-4">
        <div className="sk-200 h-8 w-32" />
        <div className="sk-200 h-10 w-10 rounded-xl" />
      </div>
    </article>
  )
}
