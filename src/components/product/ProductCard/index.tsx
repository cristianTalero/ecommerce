import Link from 'next/link'
import { Card, Divider } from 'react-daisyui'
import { Avatar } from '~/components/utils/Avatar'
import { CustomImage } from '~/components/utils/CustomImage'
import { type Category, categories } from '~/utils/products'
import { toCurrency } from '~/utils/toCurrency'

type ProductCardProps = {
  id: string
  image: string
  category: Category
  title: string
  description: string
  price: number
  owner: string | null
}

export function ProductCard({
  id,
  image,
  category,
  title,
  description,
  price,
  owner,
}: ProductCardProps) {
  return (
    <Link href={`/products/${id}`}>
      <Card className="touchable relative w-72 shrink-0 bg-base-100 shadow">
        <figure className="relative h-52 w-full">
          <CustomImage src={image} alt="Product image" fill />
        </figure>
        <Card.Body className="p-4">
          <span className="flex items-center gap-2 text-sm font-semibold capitalize text-secondary">
            {categories[category]}
            {category}
          </span>
          <Card.Title className="line-clamp-2">{title}</Card.Title>
          <p className="line-clamp-3">{description}</p>
        </Card.Body>
        <Divider className="m-0" />
        <Card.Actions className="flex items-center justify-around p-2">
          <span className="text-xl font-black text-success">
            {toCurrency(price)}
          </span>
          <Avatar src={owner} alt="Person" size={40} />
        </Card.Actions>
      </Card>
    </Link>
  )
}
