import Link from 'next/link'
import { BiPlusCircle } from 'react-icons/bi'
import type { Category } from '~/utils/products'

type ProductSectionProps = {
  category: Category
}

export function ProductSectionMore({ category }: ProductSectionProps) {
  return (
    <Link
      href={`/products/?category=${category}`}
      aria-label="Show all"
      className="touchable">
      <BiPlusCircle size={90} className="text-slate-400" />
    </Link>
  )
}
