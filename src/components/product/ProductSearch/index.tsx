import Link from 'next/link'
import { memo } from 'react'
import { Avatar } from '~/components/utils/Avatar'
import { CustomImage } from '~/components/utils/CustomImage'
import type { Category } from '~/utils/products'
import { toCurrency } from '~/utils/toCurrency'

type ProductSearchProps = {
  id: string
  image: string
  title: string
  category: Category
  price: number
  owner: string | null
}

function ProductSearchComponent({
  id,
  image,
  title,
  category,
  price,
  owner,
}: ProductSearchProps) {
  return (
    <Link href={`/products/${id}`}>
      <li className="touchable flex items-center gap-2 pr-2 shadow">
        <CustomImage src={image} alt="Product" height={70} width={70} />
        <div className="flex flex-col">
          <h4 className="text-ellipsis font-bold line-clamp-2">{title}</h4>
          <h4 className="text-ellipsis font-semibold capitalize text-primary line-clamp-2">
            {category}
          </h4>
          <h4 className="truncate font-semibold text-success">
            {toCurrency(price)}
          </h4>
        </div>
        <Avatar src={owner} alt="User avatar" size={40} className="ml-auto" />
      </li>
    </Link>
  )
}

export const ProductSearch = memo(ProductSearchComponent)
