import { Modal } from 'react-daisyui'
import { CustomImage } from '../CustomImage'

type ModalIllustrationProps = {
  illustration: string
  label: string
  alt: string
}

export function ModalIllustration({
  illustration,
  label,
  alt,
}: ModalIllustrationProps) {
  return (
    <Modal.Body className="grid h-full place-content-center place-items-center gap-4">
      <CustomImage
        src={`/ilustrations/${illustration}.svg`}
        alt={alt}
        width={200}
        height={200}
      />
      <span className="mb-2 text-xl font-semibold">{label}</span>
    </Modal.Body>
  )
}
