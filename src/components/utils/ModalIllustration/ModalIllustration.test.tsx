import { render } from '@testing-library/react'
import { Modal } from 'react-daisyui'
import { ModalIllustration } from '.'

it('should show a ilustration in a Modal', () => {
  const { getByText, getByAltText } = render(
    <Modal open>
      <ModalIllustration
        illustration="searching"
        label="Test"
        alt="Image alt"
      />
    </Modal>
  )

  expect(getByAltText('Image alt')).toBeTruthy()
  expect(getByText('Test')).toBeTruthy()
})
