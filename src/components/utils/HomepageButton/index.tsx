import clsx from 'clsx'
import Link from 'next/link'
import { Navbar } from 'react-daisyui'

export function HomepageButton() {
  return (
    <Navbar.Start>
      <Link
        href="/"
        className={clsx([
          'btn',
          'btn-ghost',
          'text-xl',
          'font-semibold',
          'normal-case',
          'text-base-100',
        ])}>
        Ecommerce
      </Link>
    </Navbar.Start>
  )
}
