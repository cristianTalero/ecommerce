import { render } from '@testing-library/react'
import { HomepageButton } from '.'

it('should renders a link button to homepage', () => {
  const { getByText } = render(<HomepageButton />)
  const link = getByText('Ecommerce')

  expect(link.nodeName).toStrictEqual('A')
  expect(link.getAttribute('href')).toStrictEqual('/')
  expect(link).toBeTruthy()
})
