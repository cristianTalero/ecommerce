import { render } from '@testing-library/react'
import { AppURI } from '.'

it('should renders component', () => {
  const { getByText } = render(<AppURI />)

  expect(getByText('ecommerce.manlou.space')).toBeTruthy()
})
