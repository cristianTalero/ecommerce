export function AppURI() {
  return (
    <span className="text-lg font-bold text-primary">
      {process.env.VERCEL_URL ?? 'ecommerce.manlou.space'}
    </span>
  )
}
