import { Mask } from 'react-daisyui'
import { CustomImage } from '../CustomImage'

type AvatarProps = {
  src: string | null | undefined
  alt: string
  size: number
  loading?: boolean
  className?: string
}

export function Avatar({
  src,
  alt,
  size,
  loading = false,
  className,
}: AvatarProps) {
  if (loading) {
    return (
      <Mask
        variant="squircle"
        className="sk-200"
        style={{ width: `${size}px`, height: `${size}px` }}
      />
    )
  }

  return (
    <CustomImage
      className={`rounded-2xl ${className}`}
      src={src ?? '/images/default/avatar.webp'}
      alt={alt}
      width={size}
      height={size}
    />
  )
}
