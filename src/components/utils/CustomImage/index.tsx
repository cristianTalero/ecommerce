import Image, { type ImageProps } from 'next/image'
import { useState, useEffect } from 'react'

const MIN_IMAGE_SIZE = 40

export function CustomImage(props: ImageProps) {
  const [imageSrc, setImageSrc] = useState(props.src)
  const isImageSmall =
    (props.height as number) < MIN_IMAGE_SIZE &&
    (props.width as number) < MIN_IMAGE_SIZE

  const isRemote = (imageSrc as string).startsWith('http', 0)

  useEffect(() => setImageSrc(props.src), [props.src])

  return (
    <Image
      {...props}
      src={imageSrc}
      alt={props.alt}
      referrerPolicy={isRemote ? 'no-referrer' : undefined}
      placeholder={isImageSmall ? 'empty' : 'blur'}
      blurDataURL={typeof props.src === 'string' ? props.src : undefined}
      unoptimized={isRemote}
      onError={() => setImageSrc('/images/default/broken-img.webp')}
      style={{ objectFit: 'cover', objectPosition: 'center' }}
    />
  )
}
