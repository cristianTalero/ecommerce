import { Modal, RadialProgress } from 'react-daisyui'
import { createPortal } from 'react-dom'

export function AsyncLoading({ label = 'Wait a moment' }) {
  return createPortal(
    <Modal open className="w-80" role="alertdialog">
      <Modal.Body className="grid h-72 place-items-center">
        <RadialProgress
          value={70}
          size="7rem"
          thickness="7px"
          color="primary"
          className="animate-spin"
        />
        <span className="animate-pulse text-xl font-semibold text-primary">
          {label}
        </span>
      </Modal.Body>
    </Modal>,
    document.getElementById('__next') as Element
  )
}
