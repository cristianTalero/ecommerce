import { render } from '@testing-library/react'
import { AsyncLoading } from '.'

let modalRoot: HTMLDivElement

beforeEach(() => {
  modalRoot = document.createElement('div')
  modalRoot.setAttribute('id', '__next')
  document.body.appendChild(modalRoot)
})

afterEach(() => document.body.removeChild(modalRoot))

it('should renders a modal with a animated spinner', () => {
  const element = render(<AsyncLoading />)

  expect(element.getByRole('alertdialog')).toBeTruthy()
  expect(element.getByText('Wait a moment')).toBeTruthy()

  const spinner = element.container.getElementsByClassName('animate-spin')
  expect(spinner).toBeTruthy()
})

it('should renders a modal with a custom label', () => {
  const element = render(<AsyncLoading label="Test" />)
  expect(element.getByText('Test')).toBeTruthy()
})
