import { useState } from 'react'
import { Button } from 'react-daisyui'

type ShowMoreTextProps = {
  moreLabel?: string
  lessLabel?: string
  content: string
  range?: number
  className?: string
}

export function ShowMoreText({
  moreLabel = 'Show more',
  lessLabel = 'Hide text',
  content,
  range = 220,
  className,
}: ShowMoreTextProps) {
  const [showMore, setShowMore] = useState(false)

  return (
    <p className={className}>
      {showMore ? content : content.substring(0, range)}

      {content.length > range && (
        <>
          {!showMore && '...'}

          <Button
            type="button"
            onClick={() => setShowMore(current => !current)}
            size="xs"
            color="secondary"
            className="mt-1 ml-1">
            {showMore ? lessLabel : moreLabel}
          </Button>
        </>
      )}
    </p>
  )
}
