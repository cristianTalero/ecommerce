import clsx from 'clsx'
import { signIn } from 'next-auth/react'

import { Button } from 'react-daisyui'
import { BsFacebook } from 'react-icons/bs'
import { FcGoogle } from 'react-icons/fc'

const providers = {
  google: {
    icon: <FcGoogle />,
    style: ['bg-base-100', 'text-label', 'border-label', 'border-3'],
  },
  facebook: {
    icon: <BsFacebook />,
    style: ['bg-blue-500'],
  },
}

type SocialButtonProps = {
  provider: keyof typeof providers
  disabled?: boolean
  onClick(): void
}

export function SocialButton({
  provider,
  disabled = false,
  onClick,
}: SocialButtonProps) {
  const data = providers[provider]

  return (
    <Button
      onClick={() => {
        onClick()
        signIn(provider)
      }}
      startIcon={data.icon}
      type="button"
      disabled={disabled}
      className={clsx(
        [
          'normal-case',
          'bg-base-100',
          'text-white',
          'touchable',
          'border-none',
          'shadow-md',
        ],
        data.style
      )}>
      Sign In with <span className="capitalize">{provider}</span>
    </Button>
  )
}
