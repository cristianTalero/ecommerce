import { render, fireEvent } from '@testing-library/react'
import { SocialButton } from '.'
import { signIn } from 'next-auth/react'

jest.mock('next-auth/react', () => ({
  signIn: jest.fn(
    (social: string) =>
      new Promise(resolve => {
        resolve(social)
      })
  ),
}))

describe('SocialButton', () => {
  afterAll(() => jest.clearAllMocks())

  it('should call signIn with Google provider when clicked', async () => {
    const provider = 'google'

    const onClick = jest.fn()
    const { getByText } = render(
      <SocialButton provider={provider} onClick={onClick} />
    )

    fireEvent.click(getByText(/^Sign In with/))

    expect(onClick).toHaveBeenCalledTimes(1)
    expect(signIn).toHaveBeenCalledWith(provider)
  })

  it('should call signIn with Facebook provider when clicked', async () => {
    const provider = 'facebook'

    const onClick = jest.fn()
    const { getByText } = render(
      <SocialButton provider={provider} onClick={onClick} />
    )

    fireEvent.click(getByText(/^Sign In with/))

    expect(onClick).toHaveBeenCalledTimes(1)
    expect(signIn).toHaveBeenCalledWith(provider)
  })
})
