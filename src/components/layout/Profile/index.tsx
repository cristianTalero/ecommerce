import { Popover } from '@headlessui/react'
import { Divider, Menu } from 'react-daisyui'
import { Avatar } from '~/components/utils/Avatar'
import { MdLogout, MdPerson } from 'react-icons/md'
import Link from 'next/link'
import { signOut } from 'next-auth/react'
import clsx from 'clsx'
import { BsPlusLg } from 'react-icons/bs'

type ProfileProps = {
  id: string
  name: string
  avatar: string | null | undefined
  loading: boolean
}

export function Profile({ id, name, avatar, loading }: ProfileProps) {
  return (
    <Popover className="relative">
      <Popover.Button className="touchable rounded-xl" disabled={loading}>
        <Avatar src={avatar} alt="Profile image" loading={loading} size={40} />
      </Popover.Button>

      <Popover.Panel className="absolute right-0 z-10">
        <div className="w-72 rounded-md bg-base-100 p-2 shadow-md">
          <h3 className="mb-3 block text-center text-lg font-black">You</h3>

          <Divider className="m-0" />
          <Menu className="flex flex-col gap-1">
            <Menu.Item>
              <Link
                href={`/users/${id}`}
                className={clsx([
                  'flex',
                  'justify-center',
                  'items-center',
                  'gap-2',
                  'p-1',
                  'normal-case',
                  'rounded-md',
                ])}>
                <Avatar src={avatar} alt="Profile image" size={50} />

                <div className="flex flex-col">
                  <h3 className="text-ellipsis font-semibold text-secondary">
                    {name}
                  </h3>
                  <h4
                    className="flex items-center gap-1 truncate text-sm font-light"
                    aria-label="User ID">
                    <MdPerson />
                    {id}
                  </h4>
                </div>
              </Link>
            </Menu.Item>

            <Divider className="m-0" />

            <Menu.Item>
              <Link href="/me/create-product" className="rounded-md">
                <BsPlusLg />
                New
              </Link>
            </Menu.Item>

            <Menu.Item>
              <div
                className="rounded-md shadow-sm"
                onClick={() => signOut({ redirect: true })}>
                <MdLogout />
                Logout
              </div>
            </Menu.Item>
          </Menu>
        </div>
      </Popover.Panel>
    </Popover>
  )
}
