import { CustomImage } from '~/components/utils/CustomImage'
import type { Category } from '~/utils/products'

type HeroProps = {
  category: Category | null
}

export function Hero({ category }: HeroProps) {
  return (
    <header className="relative grid h-96 w-full place-items-center">
      <CustomImage
        className="blur-[2px] filter"
        src={`/images/hero/${category ? category : 'main'}.webp`}
        alt="Hero image"
        fill
      />
      <h1 className="text-center text-4xl capitalize text-base-100 drop-shadow-lg">
        {category ? category : 'Everything you want'}
      </h1>
    </header>
  )
}
