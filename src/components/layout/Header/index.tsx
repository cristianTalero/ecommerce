import { useSession } from 'next-auth/react'
import Link from 'next/link'
import { Badge, Button, Indicator, Navbar, Tooltip } from 'react-daisyui'

import { BsCartFill, BsSearch } from 'react-icons/bs'
import { MdLogin } from 'react-icons/md'

import { HomepageButton } from '~/components/utils/HomepageButton'
import { Profile } from '../Profile'
import dynamic from 'next/dynamic'
import { useState } from 'react'
import { useCartStore } from '~/store/cartStore'

const Search = dynamic(
  async () => {
    const search = await import('../Search')
    return search.Search
  },
  { ssr: false }
)

const Cart = dynamic(
  async () => {
    const cart = await import('../Cart')
    return cart.Cart
  },
  { ssr: false }
)

export function Header() {
  const [showSearch, setShowSearch] = useState(false)
  const [showCart, setShowCart] = useState(false)

  const { noProducts } = useCartStore(cart => ({
    noProducts: cart.computed.isEmpty,
  }))

  const { data: session, status } = useSession()
  const user = session?.user

  return (
    <Navbar className="sticky top-0 z-30 bg-secondary" role="navigation">
      <HomepageButton />
      <Search open={showSearch} onClose={() => setShowSearch(false)} />
      <Cart open={showCart} onClose={() => setShowCart(false)} />

      <Navbar.End className="flex gap-1">
        <Button
          aria-label="Search"
          color="ghost"
          size="sm"
          onClick={() => setShowSearch(true)}>
          <BsSearch size={22} />
        </Button>

        {status === 'authenticated' ? (
          <Button
            aria-label="Your cart"
            type="button"
            onClick={() => setShowCart(true)}
            color="ghost"
            size="sm">
            <Indicator
              item={
                !noProducts ? (
                  <Badge color="error" size="xs" className="animate-pulse" />
                ) : undefined
              }
              horizontal="end"
              vertical="top">
              <BsCartFill size={22} />
            </Indicator>
          </Button>
        ) : null}

        {status === 'unauthenticated' ? (
          <Tooltip message="Sign In" position="bottom">
            <Link
              href="/auth/signin"
              className="btn-primary btn-sm btn"
              aria-label="Sign in">
              <MdLogin size={20} />
            </Link>
          </Tooltip>
        ) : (
          <Profile
            id={user?.id as string}
            name={user?.name as string}
            avatar={user?.image}
            loading={status === 'loading'}
          />
        )}
      </Navbar.End>
    </Navbar>
  )
}
