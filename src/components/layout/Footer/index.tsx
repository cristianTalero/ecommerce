import clsx from 'clsx'
import type { ReactNode } from 'react'
import { Button } from 'react-daisyui'
import { FaGitlab, FaPortrait } from 'react-icons/fa'
import { TbBrandNextjs, TbBrandVercel } from 'react-icons/tb'

type LinkProps = {
  icon: ReactNode
  href: string
  label: string
  upper: boolean
}

function Link({ icon, href, label, upper }: LinkProps) {
  return (
    <Button
      startIcon={icon}
      size="sm"
      color="ghost"
      href={href}
      className={`text-red-500 ${upper ? 'uppercase' : 'normal-case'}`}>
      {label}
    </Button>
  )
}

export function Footer() {
  return (
    <footer
      className={clsx([
        'bg-slate-800',
        'text-base-100',
        'w-full',
        'px-4',
        'py-10',
        'flex',
        'flex-col',
        'items-center',
        'text-center',
        'gap-4',
      ])}>
      <h3 className="text-2xl font-semibold">Ecommerce app</h3>
      <div className="flex gap-2">
        <Link
          icon={<FaGitlab />}
          href="https://gitlab.com/cristianTalero/ecommerce"
          label="Repository"
          upper
        />
        <Link
          icon={<FaPortrait />}
          href="https://cv.manlou.space/proyectos/ecommerce"
          label="CV"
          upper
        />
      </div>
      <p className="font-light">Copyright © All rights reserved</p>
      <p className="font-bold">
        ❤ Created by Cristian Talero with
        <Link
          icon={<TbBrandNextjs />}
          href="https://nextjs.org"
          label="NextJS"
          upper={false}
        />
        and
        <Link
          icon={<TbBrandVercel />}
          href="https://vercel.com"
          label="Vercel"
          upper={false}
        />
      </p>
    </footer>
  )
}
