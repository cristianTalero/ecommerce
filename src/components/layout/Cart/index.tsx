import { Modal, Divider } from 'react-daisyui'

import { useCartStore } from '~/store/cartStore'

import { CardItem } from './CardItem'
import { shallow } from 'zustand/shallow'

import { Fragment } from 'react'
import { toCurrency } from '~/utils/toCurrency'
import { FaDollarSign } from 'react-icons/fa'
import { ModalIllustration } from '~/components/utils/ModalIllustration'
import { useModalHandler } from '~/hooks/useModalHandler'
import { createPortal } from 'react-dom'
import Link from 'next/link'

type CartProps = {
  open: boolean
  onClose(): void
}

export function Cart({ open, onClose }: CartProps) {
  const showModal = useModalHandler(open)

  const { products, noProducts, total } = useCartStore(
    cart => ({
      products: cart.items,
      noProducts: cart.computed.isEmpty,
      total: cart.computed.total,
    }),
    shallow
  )

  const { increaseProduct, removeOneProduct } = useCartStore(cart => ({
    increaseProduct: cart.increase,
    removeOneProduct: cart.remove,
  }))

  return createPortal(
    <Modal
      open={showModal}
      responsive
      className={`p-0 ${!noProducts ? 'h-96' : 'h-auto'}`}
      onClickBackdrop={onClose}>
      <Modal.Header className="sticky top-0 mb-1 w-full rounded-md bg-base-100 py-2 px-4">
        <h3 className="text-center text-xl font-black">
          Cart&nbsp;
          <span className="text-base font-light">
            ({products.length} items)
          </span>
        </h3>
        {!noProducts ? (
          <div className="flex justify-between">
            <Link
              href="/pay/personal-information"
              className="btn-primary btn-xs btn">
              <FaDollarSign className="mr-1" />
              Pay
            </Link>
            <h4 className="text-sm font-bold text-success">
              {toCurrency(total)}
            </h4>
          </div>
        ) : null}
        <Divider className="m-0" />
      </Modal.Header>
      {!noProducts ? (
        <Modal.Body className="px-3">
          <ul className="overflow-auto p-1">
            {products.map((product, i) => (
              <Fragment key={product.id}>
                <CardItem
                  key={product.id}
                  id={product.id}
                  title={product.title}
                  image={product.image}
                  price={product.price}
                  units={product.units}
                  onIncrease={increaseProduct}
                  onRemove={removeOneProduct}
                />
                {i !== products.length ? <li className="divider m-0" /> : null}
              </Fragment>
            ))}
          </ul>
        </Modal.Body>
      ) : (
        <ModalIllustration
          illustration="empty-cart"
          label="There are nothing in your cart"
          alt="Empty cart"
        />
      )}
    </Modal>,
    document.getElementById('__next') as Element
  )
}
