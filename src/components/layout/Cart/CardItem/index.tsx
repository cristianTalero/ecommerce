import { Button, ButtonGroup } from 'react-daisyui'
import { CustomImage } from '~/components/utils/CustomImage'
import { AiFillDelete, AiOutlinePlus } from 'react-icons/ai'
import Link from 'next/link'
import { toCurrency } from '~/utils/toCurrency'
import { useCartStore } from '~/store/cartStore'

const ICON_SIZE = 17

type CardItemProps = {
  id: string
  title: string
  image: string
  price: number
  units: number
  onIncrease(_: string): void
  onRemove(_: string): void
}

export function CardItem({
  id,
  title,
  image,
  price,
  units,
  onIncrease,
  onRemove,
}: CardItemProps) {
  const { isMax } = useCartStore(cart => ({
    isMax: cart.isMax,
  }))

  const maxProducts = isMax(id)

  return (
    <li className="flex items-center justify-start gap-2">
      <Link href={`/products/${id}`}>
        <CustomImage
          className="rounded-lg"
          src={image}
          alt="Product image"
          width={50}
          height={50}
        />
      </Link>

      <div className="flex flex-col">
        <Link href={`/products/${id}`} className="link-hover font-semibold">
          {title}
        </Link>
        <span className="font-semibold text-success">
          {toCurrency(price * units)}
        </span>
        <span className="font-[400]">x{units} U.</span>
      </div>
      <ButtonGroup className="ml-auto">
        <Button
          size="xs"
          color="ghost"
          aria-label="Remove one product"
          onClick={() => onRemove(id)}>
          <AiFillDelete size={ICON_SIZE} />
        </Button>
        <Button
          size="xs"
          color="ghost"
          aria-label="Add one product"
          disabled={maxProducts}
          onClick={() => onIncrease(id)}>
          <AiOutlinePlus size={ICON_SIZE} />
        </Button>
      </ButtonGroup>
    </li>
  )
}
