import { useDeferredValue, useEffect, useMemo, useState } from 'react'
import { Modal, Input } from 'react-daisyui'
import clsx from 'clsx'
import { createPortal } from 'react-dom'

import { ModalIllustration } from '~/components/utils/ModalIllustration'
import { useModalHandler } from '~/hooks/useModalHandler'
import { ProductSearch } from '~/components/product/ProductSearch'
import { api } from '~/utils/api'

type SearchProps = {
  open: boolean
  onClose(): void
}

export function Search({ open, onClose }: SearchProps) {
  const showModal = useModalHandler(open)
  const [search, setSearch] = useState('')

  const searchMutation = api.product.search.useMutation()
  const deferredSearch = useDeferredValue(search)

  useEffect(() => {
    searchMutation.mutate({ value: deferredSearch })
  }, [deferredSearch])

  const results = useMemo(() => {
    if (!deferredSearch) return []

    return searchMutation?.data
  }, [searchMutation.data])

  const noResults = results?.length === 0 ?? true

  const getLabel = () => {
    if (!deferredSearch) return 'Write something :)'
    else if (deferredSearch && noResults) return 'Nothing to show :('
    return ''
  }

  return createPortal(
    <Modal
      responsive
      open={showModal}
      onClickBackdrop={onClose}
      className="h-[30rem] p-0">
      <Modal.Header
        className={clsx([
          noResults ? 'fixed' : 'sticky',
          'gap-2',
          'top-0',
          'm-0',
          'flex',
          'items-center',
          'justify-between',
          'bg-base-100',
          'px-3',
          'py-2',
          'shadow',
          'z-30',
        ])}>
        <Input
          type="search"
          placeholder="Search"
          className="w-full"
          bordered
          size="sm"
          borderOffset
          value={search}
          onChange={e => setSearch(e.target.value)}
        />
        <h3 className="text-sm font-bold text-label" aria-label="Results">
          {results?.length}
        </h3>
      </Modal.Header>
      {!noResults ? (
        <ul className="card-body flex flex-col gap-2 p-3">
          {results?.map(result => (
            <ProductSearch
              key={result.id}
              id={result.id}
              image={result.image}
              title={result.title}
              category={result.category}
              price={result.price}
              owner={result.owner.image}
            />
          ))}
        </ul>
      ) : (
        <ModalIllustration
          illustration="searching"
          label={getLabel()}
          alt="No reults"
        />
      )}
    </Modal>,
    document.getElementById('__next') as Element
  )
}
