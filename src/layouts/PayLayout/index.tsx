import { MainLayout } from '~/layouts/MainLayout'
import type { ReactNode } from 'react'
import { Steps } from 'react-daisyui'

type PayLayoutProps = {
  title: string
  pay: boolean
  error?: boolean
  children: ReactNode
}

export function PayLayout({ title, pay, error, children }: PayLayoutProps) {
  return (
    <MainLayout title={title}>
      <main className="grid place-items-center p-4">
        <h1 className="text-3xl font-light">Pay process</h1>

        <Steps className="my-4 text-lg font-semibold">
          <Steps.Step color="primary">
            <span className="font-light">Personal information</span>
          </Steps.Step>
          <Steps.Step color={pay ? 'primary' : undefined}>
            <span className="font-light">Pay method</span>
          </Steps.Step>
          <Steps.Step
            color={
              typeof error === 'undefined'
                ? undefined
                : error
                ? 'error'
                : 'success'
            }>
            <span className="font-light">Confirmation</span>
          </Steps.Step>
        </Steps>

        {children}
      </main>
    </MainLayout>
  )
}
