import clsx from 'clsx'
import Head from 'next/head'
import { type ReactNode } from 'react'
import { Navbar } from 'react-daisyui'
import { HomepageButton } from '~/components/utils/HomepageButton'

type StaticLayoutProps = {
  title: string
  children: ReactNode
}

export function StaticLayout({ title, children }: StaticLayoutProps) {
  return (
    <div className="bg-gradient">
      <Head>
        <title>{`${title} | Ecommerce`}</title>
      </Head>

      <Navbar
        role="navigation"
        className="sticky top-0 z-50 shadow backdrop-blur-lg">
        <HomepageButton />
      </Navbar>

      <main className="hero" style={{ height: 'calc(100vh - 132px)' }}>
        {children}
      </main>

      <footer
        className={clsx([
          'bg-neutral',
          'fixed',
          'bottom-0',
          'w-full',
          'p-6',
          'flex',
          'justify-between',
          'text-white',
          'font-semibold',
          'text-sm',
        ])}>
        Cristian Talero - ©2022
      </footer>
    </div>
  )
}
