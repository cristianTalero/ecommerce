import Head from 'next/head'
import type { ReactNode } from 'react'
import { Footer } from '~/components/layout/Footer'
import { Header } from '~/components/layout/Header'

type MainLayoutProps = {
  title: string
  children: ReactNode
}

export function MainLayout({ title, children }: MainLayoutProps) {
  return (
    <>
      <Head>
        <title>{`${title} | Ecommerce`}</title>
      </Head>

      <Header />
      {children}
      <Footer />
    </>
  )
}
