import type { PlaywrightTestConfig } from '@playwright/test'

const CI = process.env.CI

const config: PlaywrightTestConfig = {
  timeout: 60000,
  retries: 0,
  testDir: './e2e',
  webServer: {
    command: 'npm run preview',
    port: 3000,
    timeout: 120 * 1000,
    reuseExistingServer: !CI,
  },
  use: {
    baseURL: 'http://localhost:3000',
    headless: true,
    viewport: { width: 1280, height: 720 },
    ignoreHTTPSErrors: true,
    video: 'off',
    screenshot: 'off',
  },
  reporter: CI ? 'dot' : 'list',
  projects: [
    {
      name: 'Chromium',
      use: { browserName: 'chromium' },
    },
    {
      name: 'Firefox',
      use: { browserName: 'firefox' },
    },
    {
      name: 'Webkit',
      use: { browserName: 'webkit' },
    },
  ],
}

export default config
