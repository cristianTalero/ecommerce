import withPWA from 'next-pwa'
import runtimeCaching from 'next-pwa/cache.js'
import { withSuperjson } from 'next-superjson'

/**
 * Run `build` or `dev` with `SKIP_ENV_VALIDATION` to skip env validation.
 * This is especially useful for Docker builds.
 */
!process.env.SKIP_ENV_VALIDATION && (await import('./src/env/server.mjs'))

/** @type {import("next").NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  transpilePackages: ['react-daisyui'],
}

/** @type {import('next-pwa').PWAConfig} */
const pwaConfig = {
  dest: 'public',
  register: true,
  runtimeCaching,
  disable: process.env.NODE_ENV !== 'production',
}

const plugins = [withPWA(pwaConfig), withSuperjson()]

export default plugins.reduce((acc, next) => {
  return next(acc)
}, nextConfig)
