// eslint-disable-next-line @typescript-eslint/no-var-requires
const colors = require('tailwindcss/colors')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    'node_modules/daisyui/dist/**/*.js',
    'node_modules/react-daisyui/dist/**/*.js',
    './src/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      lineClamp: {
        8: '8',
        10: '10',
        12: '12',
      },
      colors: {
        label: colors.gray[500],
        skeleton: {
          100: colors.gray[300],
          200: colors.gray[400],
        },
      },
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1536px',
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/line-clamp'),
    require('tailwindcss-debug-screens'),
    require('daisyui'),
  ],
  darkMode: 'class',
  important: '#__next',
  daisyui: {
    themes: [
      {
        light: {
          primary: '#a02219',
          secondary: '#13678e',
          accent: '#06c99f',
          neutral: '#19191F',
          'base-100': '#EDEBF4',
          info: '#7BB6DB',
          success: '#33AB5F',
          warning: '#F09833',
          error: '#F1120E',
        },
        // dark: {
        //   primary: '#95a5e5',
        //   secondary: '#5c6cc9',
        //   accent: '#341299',
        //   neutral: '#181F25',
        //   'base-100': '#3E3F41',
        //   info: '#288EDC',
        //   success: '#18B973',
        //   warning: '#CC7205',
        //   error: '#EC6E6A',
        // },
      },
    ],
  },
}
