/*
  Warnings:

  - A unique constraint covering the columns `[productId,ownerId]` on the table `reviews` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "reviews_productId_ownerId_key" ON "reviews"("productId", "ownerId");
