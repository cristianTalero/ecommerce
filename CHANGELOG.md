# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.27](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.26...v0.0.27) (2023-06-09)


### ✨ Features

* new database provider added ([761dd42](https://gitlab.com/cristianTalero/ecommerce/commit/761dd42ebcdc060202efd31bd5691170de80afcd))
* web manifest fixed ([c47bb13](https://gitlab.com/cristianTalero/ecommerce/commit/c47bb13c23e51d8b58685705b1be4f469b3d10c0))

### [0.0.26](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.25...v0.0.26) (2023-05-03)


### 🐛 Bug Fixes

* fixed rating stars ([93f0c6f](https://gitlab.com/cristianTalero/ecommerce/commit/93f0c6f8e9dc130e760415d07d4c99ba617d57e9))
* fixed search result component dimensions ([3fe57d2](https://gitlab.com/cristianTalero/ecommerce/commit/3fe57d2f6de7dad9913e6687b8a0f39bd56675a0))

### [0.0.25](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.24...v0.0.25) (2023-05-03)


### 🚚 Chores

* dockerfile added ([f3d9dfb](https://gitlab.com/cristianTalero/ecommerce/commit/f3d9dfb6029172695ef760bd95a226dc4bbd0de4))


### 🐛 Bug Fixes

* fixed text overflow in product card and page ([adc6776](https://gitlab.com/cristianTalero/ecommerce/commit/adc6776bfacee51e9666c8bf2acdd6a3bb0d2978))

### [0.0.24](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.23...v0.0.24) (2023-01-28)


### ✅ Testing

* **utils/toCurrency:** unit test added ([9034046](https://gitlab.com/cristianTalero/ecommerce/commit/903404691b0ed15ad92e1edbca43487dc48f4593))
* **utils/trim:** unit test added ([249c14a](https://gitlab.com/cristianTalero/ecommerce/commit/249c14a26df0bd4add52525b53d1a130dd75d2da))


### 🐛 Bug Fixes

* **pages/me/purchased/[id]:** confirm order fixed ([fd6bb37](https://gitlab.com/cristianTalero/ecommerce/commit/fd6bb3778ce84e348a237d1bb22c246ade31cb72))

### [0.0.23](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.22...v0.0.23) (2023-01-27)


### ✨ Features

* orderby added to some routes ([e62f076](https://gitlab.com/cristianTalero/ecommerce/commit/e62f076870ddd5086a36bb21e684fa0d3e4e432e))

### [0.0.22](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.21...v0.0.22) (2023-01-27)


### ✨ Features

* **components/layout/search:** search result component added ([1724c38](https://gitlab.com/cristianTalero/ecommerce/commit/1724c38172cfdfbba238269c385c873287c2b7f8))
* **pages/me/purchased/[id]:** added productId to page ([ad96850](https://gitlab.com/cristianTalero/ecommerce/commit/ad96850081b03c3e6e2abe3610ace991d2df4253))
* **pages/me/purchased/[id]:** color added to order info ([91aa90e](https://gitlab.com/cristianTalero/ecommerce/commit/91aa90ebde172ec8d04d388847c3a5be1fd093c4))
* search functionallity added ([d981830](https://gitlab.com/cristianTalero/ecommerce/commit/d98183057c277ee05a1a59ee9699e8252e2a484d))

### [0.0.21](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.20...v0.0.21) (2023-01-26)


### 🐛 Bug Fixes

* fixed declined card order ([26c7ba5](https://gitlab.com/cristianTalero/ecommerce/commit/26c7ba559a55dc0aee576f7341dac927f537ae78))

### [0.0.20](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.19...v0.0.20) (2023-01-26)


### ⚡️ Performance Improvements

* converted images to webp format ([df2a78d](https://gitlab.com/cristianTalero/ecommerce/commit/df2a78d6fa44cbb456d6af6473abe6b93f71a438))


### 🐛 Bug Fixes

* **pages/me/create-product:** session required added ([6150669](https://gitlab.com/cristianTalero/ecommerce/commit/61506694c6956d3e02d0de1413bd6e1f4a906a69))


### ✨ Features

* added functionality to orders ([a70e28e](https://gitlab.com/cristianTalero/ecommerce/commit/a70e28e90ff02ebbbff476d73900ba230e1ccb48))
* added product data to order page ([9d5615b](https://gitlab.com/cristianTalero/ecommerce/commit/9d5615b19f5201379b4c3161b4cfbd147c17b855))
* loading indicator added to pays ([85a22fe](https://gitlab.com/cristianTalero/ecommerce/commit/85a22fede086b0e294d4d5d488e346983392e182))
* orders added (Partially) ([ad509c4](https://gitlab.com/cristianTalero/ecommerce/commit/ad509c478e27382ea31692b14b9dc436b3855b2d))
* **pages/auth/signin:** save session removed ([9bd50d8](https://gitlab.com/cristianTalero/ecommerce/commit/9bd50d8a76db4ce7b68b2558974de15031f1c007))
* pay pasarel added (without functionality) ([1f5c533](https://gitlab.com/cristianTalero/ecommerce/commit/1f5c53352112f2a3e38bf9cd614e2a965e9b93ea))
* stripe added to payments ([00759cf](https://gitlab.com/cristianTalero/ecommerce/commit/00759cf6637bffb66f80a1027be7b482e1878014))
* success and cancel order added ([2cfd106](https://gitlab.com/cristianTalero/ecommerce/commit/2cfd106c3e4de2e66e8b5bdd2fdd55ac7864bdec))

### [0.0.19](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.18...v0.0.19) (2023-01-23)


### 🐛 Bug Fixes

* fixed cards width in small screens ([53d77c6](https://gitlab.com/cristianTalero/ecommerce/commit/53d77c6af7dc326a9b13195b258cfe1f480cce23))

### [0.0.18](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.17...v0.0.18) (2023-01-23)


### 🐛 Bug Fixes

* create-product page and api route bugs fixed ([b65f57a](https://gitlab.com/cristianTalero/ecommerce/commit/b65f57abe21eca4e9ff6debd63cca4501a1a4812))


### ✨ Features

* filter removed from SectionHeader ([7c42265](https://gitlab.com/cristianTalero/ecommerce/commit/7c422653237f4fd66d376921f6fe0eca8106b2b2))
* **pages/products:** products by category page added ([1061d66](https://gitlab.com/cristianTalero/ecommerce/commit/1061d66274d138931a8632de0a9bf09520be4646))
* products by category finished ([c6b37cb](https://gitlab.com/cristianTalero/ecommerce/commit/c6b37cb3bbe5fbaca39a40e3c7b120ea0d1057ea))

### [0.0.17](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.16...v0.0.17) (2023-01-21)


### ✨ Features

* favorite products section removed ([ff15944](https://gitlab.com/cristianTalero/ecommerce/commit/ff15944a2beb98e4fc07da02c63885645cb14a6e))

### [0.0.16](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.15...v0.0.16) (2023-01-21)

### [0.0.15](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.14...v0.0.15) (2023-01-21)

### ✨ Features

- crud operations added to review entity ([98d6547](https://gitlab.com/cristianTalero/ecommerce/commit/98d6547ae597084c6d1b68638512550ecf6ef754))
- **pages/users/[id]:** delete product added ([03d7465](https://gitlab.com/cristianTalero/ecommerce/commit/03d74656661c1f5fdd0afab804221079963fbbe2))

### [0.0.14](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.13...v0.0.14) (2023-01-20)

### ♻️ Code Refactoring

- review form and rating card in the same section now ([e3ce2d9](https://gitlab.com/cristianTalero/ecommerce/commit/e3ce2d9070a085801370cafce14a9402e1e7e56d))

### 🚚 Chores

- updatedAt migration added to review model ([c7280f6](https://gitlab.com/cristianTalero/ecommerce/commit/c7280f6c1f6160fac4add3df58bc62a796993b82))

### ✨ Features

- createReview added in backend ([3ef924d](https://gitlab.com/cristianTalero/ecommerce/commit/3ef924d17d6ee3e5c953f01bafb14f97c6a3ca48))
- **pages/products/[id]:** client fetch of reviews added to page ([c43332b](https://gitlab.com/cristianTalero/ecommerce/commit/c43332bbe7888b2cd18089c7a5b5e898293b5070))
- **pages/products/[id]:** rating card is working ([7c3c763](https://gitlab.com/cristianTalero/ecommerce/commit/7c3c763b3f0f8f57249bb2c5e41be277cca64a5c))
- **pages/products/[id]:** server side review will show if user already did it ([78df370](https://gitlab.com/cristianTalero/ecommerce/commit/78df37088702e53a3a5dd7876885fda74eb35db7))
- **pages/products/[id]:** skeleton placeholder added to rating card ([0db465c](https://gitlab.com/cristianTalero/ecommerce/commit/0db465cac769510fea0f534099017c4d697c8992))
- product form created ([3726945](https://gitlab.com/cristianTalero/ecommerce/commit/3726945d23f09d905a9c47ab52a8896cfc97d3e4))
- rating added to product page ([8a293da](https://gitlab.com/cristianTalero/ecommerce/commit/8a293daa5948c004d6a6daec486fef3a5c3d5998))
- review model migration added ([4dc5392](https://gitlab.com/cristianTalero/ecommerce/commit/4dc5392230ca530b24c32be40aa68aeb52104208))

### 🐛 Bug Fixes

- background color of cards fixed ([2f1520f](https://gitlab.com/cristianTalero/ecommerce/commit/2f1520f6fd8ee00ec0af74d68a9991f54ca947f4))
- **components/rating/RatingCard:** fixed stars inverted bug ([cd788f0](https://gitlab.com/cristianTalero/ecommerce/commit/cd788f0338da9edb6746ba0b3af3d9cb45655cfc))
- **components/rating/RatingForm:** fixed stars bug when trying to create a review ([d19388e](https://gitlab.com/cristianTalero/ecommerce/commit/d19388e9230235d9a5af690f25146ebed6c99dc9))
- fixed responsive dimensions of rating components ([682af68](https://gitlab.com/cristianTalero/ecommerce/commit/682af6882947bdea89a081f35f5e1d91c6329f90))

### [0.0.13](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.12...v0.0.13) (2023-01-18)

### 🚚 Chores

- **release:** 0.0.12 ([f93639b](https://gitlab.com/cristianTalero/ecommerce/commit/f93639bb0904fac1d293cc929e8bf0aa4e61bd01))

### 🐛 Bug Fixes

- **pages/users/[id]:** prisma is not defined fixed ([868dbba](https://gitlab.com/cristianTalero/ecommerce/commit/868dbbade0cda21bdb7165370e82b92309b60593))

### [0.0.12](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.11...v0.0.12) (2023-01-18)

### 💄 Styling

- lint errors fixed ([9d5f15c](https://gitlab.com/cristianTalero/ecommerce/commit/9d5f15c11cafd44c4ff6a5c3aff1a531146050d6))

### [0.0.11](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.10...v0.0.11) (2023-01-18)

### ✨ Features

- added status component to `user` page ([26fbdd6](https://gitlab.com/cristianTalero/ecommerce/commit/26fbdd64b3c9e1f42a4da32f6126e2abd668e46d))
- category added to product entity ([5a39ff7](https://gitlab.com/cristianTalero/ecommerce/commit/5a39ff71dc6f1c5198102e7e2c4d7748bb18b71a))
- **pages/me/create-product:** error, successand loading indicator added to page when submit ([21612ee](https://gitlab.com/cristianTalero/ecommerce/commit/21612ee5b42897f5558f1082e1e0fdd5cbcb556b))
- **pages/product/[id]:** added ssr to product page ([f82fd5a](https://gitlab.com/cristianTalero/ecommerce/commit/f82fd5a325a48d44d03d310acbc76968abe460b6))
- **pages/users/[id]:** added fetch of user products ([b3976d1](https://gitlab.com/cristianTalero/ecommerce/commit/b3976d1ae4c1808a116ae090c74fac9b395787b7))

### 🐛 Bug Fixes

- **components/utils/AsyncLoading:** fixed body scroll when modal is showed ([d48cd88](https://gitlab.com/cristianTalero/ecommerce/commit/d48cd8856626570027ba80a626fdf0b6af0a4479))
- **pages/products/[id]:** image width fixed ([33bab19](https://gitlab.com/cristianTalero/ecommerce/commit/33bab19b7ce847505300f72722ed058d64af3847))
- **store/cartStore:** fixed dd bug when product max is zero (0) ([afd58b7](https://gitlab.com/cristianTalero/ecommerce/commit/afd58b7728f6675fe3d5cb761c09f3c1e1d18b5b))

### [0.0.10](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.9...v0.0.10) (2023-01-18)

### 🐛 Bug Fixes

- **pages/user/[id]:** user data dimensions fixed ([e031bb6](https://gitlab.com/cristianTalero/ecommerce/commit/e031bb63608e7bf9db9bc8a0b8590fd3d835cfda))

### 🚚 Chores

- timestaps removed from user db model ([361e133](https://gitlab.com/cristianTalero/ecommerce/commit/361e1337ecb9ccc95b430c7a701658599024cf56))

### ♻️ Code Refactoring

- `schemas` folder added to contains `trpc` models ([4d248c1](https://gitlab.com/cristianTalero/ecommerce/commit/4d248c1441057af09c438594936add6f4690af24))

### ✨ Features

- 'create product' action added ([ea54303](https://gitlab.com/cristianTalero/ecommerce/commit/ea54303582b044da916b4086e0d95ad7f2d28eda))
- 'user' route will be 'users' now ([50952dc](https://gitlab.com/cristianTalero/ecommerce/commit/50952dc4b5c0d87e92f6510fc3f26de3ac3c30cf))
- **pages/auth/signin:** loader indicator while signin added ([e76d26e](https://gitlab.com/cristianTalero/ecommerce/commit/e76d26e096f0c0cb5ae64e767d8390ec0545d6d0))
- **pages/me/create-product:** product form added ([eb2d192](https://gitlab.com/cristianTalero/ecommerce/commit/eb2d192fc709b91acc878bd0c8f26ee7ae84e79c))
- **pages/users/[id]:** server side page added ([c57e364](https://gitlab.com/cristianTalero/ecommerce/commit/c57e364d771518e940ed919370db6944f17ebdea))
- product model added to database ([17565d3](https://gitlab.com/cristianTalero/ecommerce/commit/17565d33b79c772d09ac0b20e05b698b2be7a52f))
- redirection added to uth pages when user is authenticated or not ([d56e867](https://gitlab.com/cristianTalero/ecommerce/commit/d56e8672f0533f05719029d27dcbba5fdeba262d))
- verified field removed from user ([9361fc3](https://gitlab.com/cristianTalero/ecommerce/commit/9361fc382bba3e4eb89e36765b0aefcfddb56164))

### [0.0.9](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.8...v0.0.9) (2023-01-17)

### 💄 Styling

- **components/layout/Profile:** profile link align fixed ([e64d029](https://gitlab.com/cristianTalero/ecommerce/commit/e64d02966eff053361da0b9e1fd077bc94ab8ade))

### 🐛 Bug Fixes

- **pages/me:** added a responsive font-size for user name ([ea99cd7](https://gitlab.com/cristianTalero/ecommerce/commit/ea99cd7a9b548f12061337a7d9d3aa82f65db84d))

### ♻️ Code Refactoring

- **pages/user/[id]:** 'me' page replaced for 'user/[id] page ([444b63f](https://gitlab.com/cristianTalero/ecommerce/commit/444b63fb3a835190731a699ea88721e9c45e4d43))

### [0.0.8](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.7...v0.0.8) (2023-01-17)

### 🐛 Bug Fixes

- **components/utils/Avatar:** profile image size in small screens fixed ([b22a253](https://gitlab.com/cristianTalero/ecommerce/commit/b22a253d5f98fb8971b9fe03212d2084a38d5759))

### [0.0.7](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.6...v0.0.7) (2023-01-16)

### ♻️ Code Refactoring

- **utils/products.tsx:** icon property removed from category ([b4f3952](https://gitlab.com/cristianTalero/ecommerce/commit/b4f39525d5db6810b55ff28eec4a021e0ed7097e))

### ✨ Features

- **hooks/useModalHandler:** page overflow will be hidden when modal is open ([ee5681b](https://gitlab.com/cristianTalero/ecommerce/commit/ee5681b5369dc773c6c58367070d54a2a2739094))
- instagram oauth provider removed ([03a799f](https://gitlab.com/cristianTalero/ecommerce/commit/03a799f5ec3089489e3747f53996037adfe24bf8))
- **pages/products/[id]:** 'add to cart' and 'remove from cart' button added ([82f6e1c](https://gitlab.com/cristianTalero/ecommerce/commit/82f6e1c014343a349a1c2a4ffdbe9e9ce11d05cb))
- **pages/products/[id]:** brand form removed ([337a669](https://gitlab.com/cristianTalero/ecommerce/commit/337a6697ec481865f5f8676cc19adb6af286e42d))
- **pages/products/[id]:** product page added ([025cf45](https://gitlab.com/cristianTalero/ecommerce/commit/025cf4531d561de17a87b8c7712721af245ed872))
- **store/cartStore:** product id changed from number to string type ([e30857c](https://gitlab.com/cristianTalero/ecommerce/commit/e30857cc542b75160d1fa5b92b2346f80854138c))

### 🚚 Chores

- **release:** 0.0.5 ([542675a](https://gitlab.com/cristianTalero/ecommerce/commit/542675a96441f89fcc38a599f28b1997be472e08))
- **release:** 0.0.6 ([a61c489](https://gitlab.com/cristianTalero/ecommerce/commit/a61c4899b8221389b5977cddc702c483a343d348))

### 🐛 Bug Fixes

- **components/layout/header:** cart menu showing error fixed ([ac38a75](https://gitlab.com/cristianTalero/ecommerce/commit/ac38a75910345caac258e0e1d362dd44ac639e41))
- **components/layout/Profile:** component dimensions fixed ([6740de8](https://gitlab.com/cristianTalero/ecommerce/commit/6740de8b4c1989b156910d6b14f389f5c44d98eb))
- hover added to profile link in popover ([1ca6a4c](https://gitlab.com/cristianTalero/ecommerce/commit/1ca6a4c3b4960986b62e7e39741e42ff54feac35))
- **layouts/StaticLayout:** layout elements position were fixed ([176014c](https://gitlab.com/cristianTalero/ecommerce/commit/176014cd36107a11fcdec4990c69ee3b7bb78560))
- **pages/me:** page header dimensions fixed ([14e2d80](https://gitlab.com/cristianTalero/ecommerce/commit/14e2d80de7510af1abc87ba87f9cffdc8b89f902))
- **store/cartStore:** 'add product' error fixed ([9abb3c4](https://gitlab.com/cristianTalero/ecommerce/commit/9abb3c410e465098fa4417c901e2f5a5cea1b14b))

### [0.0.6](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.5...v0.0.6) (2023-01-16)

### ♻️ Code Refactoring

- **utils/products.tsx:** icon property removed from category ([8e0a422](https://gitlab.com/cristianTalero/ecommerce/commit/8e0a4221d5dc8dd23dc29c82812c73e75b338a0c))

### 🐛 Bug Fixes

- hover added to profile link in popover ([b313ef1](https://gitlab.com/cristianTalero/ecommerce/commit/b313ef1b7b4fc3f4fd9130548198bc46c120175f))
- **store/cartStore:** 'add product' error fixed ([f16fe71](https://gitlab.com/cristianTalero/ecommerce/commit/f16fe711ab26e7ab4728dbbd41adb2befa3a4f33))

### ✨ Features

- **hooks/useModalHandler:** page overflow will be hidden when modal is open ([bda4850](https://gitlab.com/cristianTalero/ecommerce/commit/bda48509f651c219fb54a967ec0d42d7e7e1514a))
- **pages/products/[id]:** 'add to cart' and 'remove from cart' button added ([8e6c9f0](https://gitlab.com/cristianTalero/ecommerce/commit/8e6c9f0566e435888a5d3f76afb8443d91a6c2c4))
- **pages/products/[id]:** brand form removed ([62ef68a](https://gitlab.com/cristianTalero/ecommerce/commit/62ef68a3736c16f0da46900b830293df70c8b899))
- **pages/products/[id]:** product page added ([9775c0e](https://gitlab.com/cristianTalero/ecommerce/commit/9775c0eca05e68ee960bddc1bab271840a1fe577))
- **store/cartStore:** product id changed from number to string type ([d7e5dae](https://gitlab.com/cristianTalero/ecommerce/commit/d7e5dae4a8816b0d5925ed8f4e04ea874bb2ed8f))

### [0.0.5](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.4...v0.0.5) (2023-01-16)

### ✨ Features

- instagram oauth provider removed ([4c11bcb](https://gitlab.com/cristianTalero/ecommerce/commit/4c11bcbfb4863bf2042e5c69be92014f45a29961))

### 🐛 Bug Fixes

- **components/layout/Profile:** component dimensions fixed ([1a80adf](https://gitlab.com/cristianTalero/ecommerce/commit/1a80adfc2471990a897a00032c53ee2f2bb5aeb0))
- **layouts/StaticLayout:** layout elements position were fixed ([0cd5450](https://gitlab.com/cristianTalero/ecommerce/commit/0cd54507a0f63565d7064744be5b2d8f28e1cdce))
- **pages/me:** page header dimensions fixed ([c4fac9c](https://gitlab.com/cristianTalero/ecommerce/commit/c4fac9cb4a71dd9236ee48f131334c88a7a5ac32))

### [0.0.4](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.3...v0.0.4) (2023-01-15)

### 🚚 Chores

- 'postinstall' script removed ([95e1788](https://gitlab.com/cristianTalero/ecommerce/commit/95e1788a987c7a2c2bbdc2986844a219e563a022))

### 💄 Styling

- footer elements were centered ([32e7029](https://gitlab.com/cristianTalero/ecommerce/commit/32e702960c2c9cbe1c072e6384f8d06f47a26b7d))

### ✨ Features

- history section 'more' link removed ([68c5402](https://gitlab.com/cristianTalero/ecommerce/commit/68c5402c805edb06938c60da841da8b1fb5b9321))

### 🐛 Bug Fixes

- 'me' page header dimensions fixed ([7bb2aa8](https://gitlab.com/cristianTalero/ecommerce/commit/7bb2aa89b68de72db7d43bf64058ecb5d568d94a))
- database migration resolved ([b862334](https://gitlab.com/cristianTalero/ecommerce/commit/b8623346c36ff20ed166508523584d562c979a4d))
- database migration resolved ([9da8f67](https://gitlab.com/cristianTalero/ecommerce/commit/9da8f67c0c2253076838ef58da073f5de0eab647))
- instagram signin error fixed (partially) ([68e3428](https://gitlab.com/cristianTalero/ecommerce/commit/68e3428a6e79c5bf3e8338bf1231de983fe5c5a5))
- named imports added to zustand ([b221e7b](https://gitlab.com/cristianTalero/ecommerce/commit/b221e7b7eae38b6b94462471cc77f04186a2ebee))
- **pages/products/index.tsx:** products section align fixed ([32c3c47](https://gitlab.com/cristianTalero/ecommerce/commit/32c3c47b91b01682dd2a043ff2699937a8bc6e85))
- profile modal showing when click loading placeholder fixed ([3a35230](https://gitlab.com/cristianTalero/ecommerce/commit/3a35230a9b9637c5bc0a3cb209beda476a55c806))

### [0.0.3](https://gitlab.com/cristianTalero/ecommerce/compare/v0.0.2...v0.0.3) (2023-01-14)

### 🐛 Bug Fixes

- error when trying to signin with instagram partially fixed ([b450e46](https://gitlab.com/cristianTalero/ecommerce/commit/b450e46ae2dc9429da36b9cee1d01f08a51072af))
- user name error when email signin fixed ([cdb2a9c](https://gitlab.com/cristianTalero/ecommerce/commit/cdb2a9c8f55c3fc175258eb09cdd360f0e52d4d9))

### 0.0.2 (2023-01-14)

### ✨ Features

- cart component converted in a modal (instead popover) ([326a688](https://gitlab.com/cristianTalero/ecommerce/commit/326a68821f8a7384597f1105cc97a39afa67a27c))
- custom transformer added to trpc ([9fc1c78](https://gitlab.com/cristianTalero/ecommerce/commit/9fc1c78a3d4bd389aa94e6edfe54a23714aa8090))
- header added ([2816b79](https://gitlab.com/cristianTalero/ecommerce/commit/2816b79984cfb7ed614cb56bffd176c1861cdd3d))
- modal handler hook added ([bc17f31](https://gitlab.com/cristianTalero/ecommerce/commit/bc17f31abdc2a4d853e199b7ff7484f3ec705bcd))
- oauth added ([8310f0e](https://gitlab.com/cristianTalero/ecommerce/commit/8310f0eb17d31f6611e8057a28bc5bde50cd8aed))
- oauth added ([3704668](https://gitlab.com/cristianTalero/ecommerce/commit/37046684028da9fb3f4fab2fbf32520606b0b8f3))
- search component added (Without real functionality) ([eb4f9e2](https://gitlab.com/cristianTalero/ecommerce/commit/eb4f9e2cfe067d70369960fd765fb150b6ce79b6))

### 🚚 Chores

- cart component finished ([4e2dc1d](https://gitlab.com/cristianTalero/ecommerce/commit/4e2dc1d6e5b59f47f6a2d47cc00a922e0442323e))
- discord auth provider removed ([7041602](https://gitlab.com/cristianTalero/ecommerce/commit/704160268099f0920d79fcea6df18aabd44dd761))
- **package.json:** react-icons and clsx packages added ([52185b9](https://gitlab.com/cristianTalero/ecommerce/commit/52185b97d36b08c9a7c4a2ce22c2848b0f824a96))

### ⚡️ Performance Improvements

- preact added for builds ([eb644bb](https://gitlab.com/cristianTalero/ecommerce/commit/eb644bbfcc4455fbc10c3025cdc28604bb200cc6))
- session refetch onwindowfocus removed ([aeccbd4](https://gitlab.com/cristianTalero/ecommerce/commit/aeccbd46f88b12c0ac1d990c1cb7862dfabb6690))

### 🐛 Bug Fixes

- **components/Search:** extra-scroll when 'noResutls' removed ([a2b3970](https://gitlab.com/cristianTalero/ecommerce/commit/a2b397095c1c509918a0314f5c0d5406671eb043))
- preact removed to fix interactions bug in production ([0952a27](https://gitlab.com/cristianTalero/ecommerce/commit/0952a270f858f1bf1d6ecd69ed515ae995d3461f))
