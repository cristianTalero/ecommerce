import { test, expect } from '@playwright/test'

test('Example test', async ({ page }) => {
  await page.goto('/')
  await expect(page).toHaveURL(/http:\/\/localhost:3000\//)
})
