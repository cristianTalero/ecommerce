module.exports = {
  '/src/**/*.(tsx|ts|js)': () => 'npx tsc --noEmit',

  'src/**/*.(tsx|js|ts)': filenames => [
    `npx eslint --fix ${filenames.join(' ')}`,
    `npx prettier --write ${filenames.join(' ')}`,
  ],

  '**/*.(md|json|yml|yaml)': filenames =>
    `npx prettier --write ${filenames.join(' ')}`,
}
